-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2017 at 10:37 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smart_energy`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_data`
--

CREATE TABLE `api_data` (
  `id` bigint(100) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `channelId` varchar(50) DEFAULT NULL,
  `deviceId` varchar(200) DEFAULT NULL,
  `averageVoltageInVolts` double DEFAULT NULL,
  `averageCurrentInAmps` double DEFAULT NULL,
  `averageApparentPowerInVoltAmps` double DEFAULT NULL,
  `averageRealPowerInWatts` double DEFAULT NULL,
  `energyUsedInWattHours` double DEFAULT NULL,
  `phasePowerInVoltAmps` double DEFAULT NULL,
  `shapePowerInVoltAmps` double DEFAULT NULL,
  `minInstantInWatts` double DEFAULT NULL,
  `maxInstantInWatts` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `ctMultiplier` double NOT NULL,
  `intervalStart` datetime DEFAULT NULL,
  `units` varchar(50) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_data`
--
ALTER TABLE `api_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_data`
--
ALTER TABLE `api_data`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
