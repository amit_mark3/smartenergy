<?php
//30 * * * * home/path/to/command/webservices.php
   //date_default_timezone_set("Asia/Kolkata");
   if(!ini_get('safe_mode')) {
		set_time_limit(240);
		ini_set('memory_limit', '16M');
		ini_set('upload_max_filesize', '10M');
		ini_set('post_max_size', '10M');
		ini_set('max_input_time', 300);
		ini_set ('max_execution_time', 9900); 
		ini_set( 'upload_max_size' , '20M' );
	}
	$DBServerName = 'localhost';
    $DBName = "g566524_smartenergy";
    $DBUserName = "g566524_smart";
    $DBPwd = "smart@123*";
	$obj_connection = new mysqli($DBServerName, $DBUserName, $DBPwd , $DBName);
	
	//if(mysqli_connect_errno())	die("Unable to connect to database server.\n" . mysqli_connect_error());
	//$post['providerId'] = '7';	
	//$cparam['data'] = json_encode($post);
	//$cparam['method'] = 'POST';
	$cparam['username'] = 'GS_Ireland';		
	$cparam['password'] = 'G$_1r31@nd';
	$cparam['url'] = 'https://ma.netthings.technology/basic/rawData?providerId=7';
	//Get data using curl
	$json_obj = CallAPI($cparam);
	//print'<pre>';print_r($json_obj );print'</pre>';die;
	//$data_result = json_decode($json_obj,true);
	//$data_result=file_get_contents('apidata.txt');
	//$json_obj=file_get_contents('apidata.json');
	$data_result = json_decode($json_obj,true);
	$value='';
	// Save Data in Database
	if(count($data_result)>0){
		$save_data=	saveApiData($data_result,$obj_connection);
	}	
	
	//echo $data_result->page;
	//print'<pre>';print_r($data_result);print'</pre>';
	
// Start for Call API using curl V20170920
function CallAPI($param){
	//print'<pre>';print_r($param);print'</pre>';die;
	$login = $param['username'];
	$password = $param['password'];
	
	//$method = $param['method'];
	//$data = $param['data']!=''?$param['data']:false;
	$url = $param['url'];
	$curl = curl_init();
	
	/*
	switch ($method)
	{
		case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);

			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
		case "PUT":
			curl_setopt($curl, CURLOPT_PUT, 1);
			break;
		default:
			if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
	}*/

	// Optional Authentication:

	curl_setopt($curl, CURLOPT_TIMEOUT, 1000000000); 
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	//curl_setopt($curl, CURLOPT_USERPWD, "username:password");
	curl_setopt($curl, CURLOPT_USERPWD, "$login:$password");
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($curl);

	//curl_close($curl);
	if (curl_errno($curl)) { 
            print "Error: " . curl_error($curl); 
        } else { 
            // Show me the result 
           // var_dump($result);
			return $result;			
            curl_close($curl); 
        } 
	
}
// End for Call API using curl V20170920

// Start for Save API Data V20170920
function saveApiData($data_result,$obj_connection){
	$values='';
	if(($data_result['page']>0) && ($data_result['pageSize']>0)){
		$dataitems_array = $data_result['items'];		
		if(count($dataitems_array)>0){
			foreach ($dataitems_array as $items_data){
				//$timestamp = str_replace('Z','',str_replace('T',' ',$items_data['timestamp']));
				//echo $items_data['updatedOn'].'----'.$updatedOn."<br/>";
				$timestamp='';
				if(isset($items_data['timestamp']) && trim($items_data['timestamp'])!=''){
					$timestamp = str_replace('Z','',str_replace('T',' ',$items_data['timestamp']));
				}
				$intervalStart='';
				if(isset($items_data['intervalStart']) && trim($items_data['intervalStart'])!=''){
					$intervalStart = str_replace('Z','',str_replace('T',' ',$items_data['intervalStart']));
				}
				$createdOn ='';
				if(isset($items_data['createdOn']) && trim($items_data['createdOn'])!=''){
					$createdOn = str_replace('T',' ',str_replace('+00:00', '', gmdate('c', strtotime($items_data['createdOn']))));
				}
				$updatedOn ='';
				if(isset($items_data['updatedOn']) && trim($items_data['updatedOn'])!=''){
					$updatedOn = str_replace('T',' ',str_replace('+00:00', '', gmdate('c', strtotime($items_data['updatedOn']))));
				}	
				$channelId = isset($items_data['channelId'])?$items_data['channelId']:'';
				$deviceId = isset($items_data['deviceId'])?$items_data['deviceId']:'';
				$averageVoltageInVolts = isset($items_data['averageVoltageInVolts'])?$items_data['averageVoltageInVolts']:'';
				$averageCurrentInAmps = isset($items_data['averageCurrentInAmps'])?$items_data['averageCurrentInAmps']:'';
				$averageApparentPowerInVoltAmps = isset($items_data['averageApparentPowerInVoltAmps'])?$items_data['averageApparentPowerInVoltAmps']:'';
				$averageRealPowerInWatts = isset($items_data['averageRealPowerInWatts'])?$items_data['averageRealPowerInWatts']:'';
				$energyUsedInWattHours = isset($items_data['energyUsedInWattHours'])?$items_data['energyUsedInWattHours']:'';
				$phasePowerInVoltAmps = isset($items_data['phasePowerInVoltAmps'])?$items_data['phasePowerInVoltAmps']:'';
				$shapePowerInVoltAmps = isset($items_data['shapePowerInVoltAmps'])?$items_data['shapePowerInVoltAmps']:'';
				$minInstantInWatts = isset($items_data['minInstantInWatts'])?$items_data['minInstantInWatts']:'';
				$maxInstantInWatts = isset($items_data['maxInstantInWatts'])?$items_data['maxInstantInWatts']:'';
				$cost = isset($items_data['cost'])?$items_data['cost']:'';
				$ctMultiplier = isset($items_data['ctMultiplier'])?$items_data['ctMultiplier']:'';
				
				$values.="('".$timestamp."','".$channelId."','".$deviceId."','".$averageVoltageInVolts."','".$averageCurrentInAmps."','".$averageApparentPowerInVoltAmps."','".$averageRealPowerInWatts."','".$energyUsedInWattHours."','".$phasePowerInVoltAmps."','".$shapePowerInVoltAmps."','".$minInstantInWatts."','".$maxInstantInWatts."','".$cost."','".$ctMultiplier."','".$intervalStart."','".$createdOn."','".$updatedOn."'),";
				
			}
			//$dataToSave = array();
			//$dataToSave["timestamp"] = $timestamp;
			//$response = $db->insert("api_data", $dataToSave);
	  
			$str_value = rtrim($values,',');
			$query="INSERT INTO api_data(`timestamp`,`channelId`,`deviceId`,`averageVoltageInVolts`,`averageCurrentInAmps`,`averageApparentPowerInVoltAmps`,`averageRealPowerInWatts`,`energyUsedInWattHours`,`phasePowerInVoltAmps`,`shapePowerInVoltAmps`,`minInstantInWatts`,`maxInstantInWatts`,`cost`,`ctMultiplier`,`intervalStart`,`createdOn`,`updatedOn`) values ".$str_value;
			//echo $query;
			
			if(!mysqli_query($obj_connection, $query)){
				echo $data["response"] = "ERROR";
				//$data["error"] = mysqli_error($obj_connection);
				//$data["sql"] = $sql;
			}else{
				echo $data["response"] = "SUCCESS";
				//$data["count"] = mysqli_affected_rows($obj_connection);
				//$data["sql"] = $sql;
			}
			//$subNews = $db->query("query", $query);
			//print'<pre>';print_r($data );print'</pre>';die;
		}
	}
	
}
// End for Save API Data V20170920

?>