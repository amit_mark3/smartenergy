<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Common_model extends CI_Model {

	private $_recursive_array = array();

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		parent::__construct();
	} 

	/** 
	 * get_countries
	 *
	 * gets the countries list to display  
	 * @access public 
	 * @param int 
	 * @param int 
	 * @param string 
	 * @param string 
	 * @param int 
	 * @param array 
	 * @return array 
	 */
	public function get_countries($limit, $start, $sort_by, $sort_order, $id = FALSE, $where_clause = array(), $where_string = '')
	{  
		$sort_order 	= ($sort_order === 'desc') ? 'desc' : 'asc';
		$sort_columns 	= array('id', 'name');
		$sort_by 		= (in_array($sort_by, $sort_columns)) ? $sort_by : 'c.id';

  		$this->db->select('
  			SQL_CALC_FOUND_ROWS NULL AS found_rows, 
			c.id AS id,   
			c.name AS name 
		', FALSE); 
		
		$this->db->from('countries c');   

 		if($id !== FALSE)
		{
			$this->db->where('c.id', $id);
		}  
		else
		{
			if(sizeof($where_clause))
			{
				$this->db->or_like($where_clause);
			}
			elseif($where_string !== '')
			{
	    		$this->db->where($where_string, NULL, FALSE); 
			}   
		}  
	
		$this->db->order_by($sort_by, $sort_order);
		if($limit > 0) $this->db->limit($limit, $start); 
		$query = $this->db->get(); 
		$result = $query->result_array();
		$this->_found_rows = $this->db->query('SELECT FOUND_ROWS() AS count;')->row()->count;
		return $result;
	}  
	 
	/** 
	 * get_states
	 *
	 * gets the states list to display  
	 * @access public 
	 * @param int 
	 * @param int 
	 * @param string 
	 * @param string 
	 * @param int 
	 * @param array 
	 * @return array 
	 */
	public function get_states($limit, $start, $sort_by, $sort_order, $id = FALSE, $where_clause = array(), $where_string = '')
	{  
		$sort_order 	= ($sort_order === 'desc') ? 'desc' : 'asc';
		$sort_columns 	= array('id', 'name', 'status');
		$sort_by 		= (in_array($sort_by, $sort_columns)) ? $sort_by : 's.id';

  		$this->db->select('
  			SQL_CALC_FOUND_ROWS NULL AS found_rows, 
			s.id AS id,   
			s.name AS name, 
 			s.created AS created,
 			s.updated AS updated,
 			s.status AS status 
		', FALSE); 
		
		$this->db->from('states s');   

 		if($id !== FALSE)
		{
			$this->db->where('s.id', $id);
		}  
		else
		{
			if(sizeof($where_clause))
			{
				$this->db->or_like($where_clause);
			}
			elseif($where_string !== '')
			{
	    		$this->db->where($where_string, NULL, FALSE); 
			}   
		}  
	
		$this->db->order_by($sort_by, $sort_order);
		if($limit > 0) $this->db->limit($limit, $start); 
		$query = $this->db->get(); 
		$result = $query->result_array();
		$this->_found_rows = $this->db->query('SELECT FOUND_ROWS() AS count;')->row()->count;
		return $result;
	}  


	 
	/** 
	 * get_districts
	 *
	 * gets the states list to display  
	 * @access public 
	 * @param int 
	 * @param int 
	 * @param string 
	 * @param string 
	 * @param int 
	 * @param array 
	 * @return array 
	 */
	public function get_districts($limit, $start, $sort_by, $sort_order, $id = FALSE, $where_clause = array(), $where_string = '')
	{  
		$sort_order 	= ($sort_order === 'desc') ? 'desc' : 'asc';
		$sort_columns 	= array('id', 'name', 'status');
		$sort_by 		= (in_array($sort_by, $sort_columns)) ? $sort_by : 'd.id';

  		$this->db->select('
  			SQL_CALC_FOUND_ROWS NULL AS found_rows, 
			d.id AS id,   
			d.name AS name, 
 			d.created AS created,
 			d.updated AS updated,
 			d.status AS status 
		', FALSE); 
		
		$this->db->from('districts d');   

 		if($id !== FALSE)
		{
			$this->db->where('d.id', $id);
		}  
		else
		{
			if(sizeof($where_clause))
			{
				$this->db->or_like($where_clause);
			}
			elseif($where_string !== '')
			{
	    		$this->db->where($where_string, NULL, FALSE); 
			}   
		}  
	
		$this->db->order_by($sort_by, $sort_order);
		if($limit > 0) $this->db->limit($limit, $start); 
		$query = $this->db->get(); 
		$result = $query->result_array();
		$this->_found_rows = $this->db->query('SELECT FOUND_ROWS() AS count;')->row()->count;
		return $result;
	}  

	/** 
	 * insert_record_in_table
	 *
	 * insert a new record in a table
	 * 
	 * @access public 
	 * @param  array
	 * @param  string
	 * @return int 
	 */
	public function insert_record_in_table($table, $post_data)
	{
		if($this->db->insert($table, $post_data))
			return  $this->db->insert_id();
		return FALSE;		
	} 

	/** 
	 * insert_batch_record_in_table
	 *
	 * insert a new record in a table
	 * 
	 * @access public 
	 * @param  array
	 * @param  string
	 * @return int 
	 */
	public function insert_batch_record_in_table($table, $post_batch_data)
	{
		if($this->db->insert_batch($table, $post_batch_data))
			return  TRUE;
		return FALSE;		
	}  

	/** 
	 * delete_records_from_table
	 *
	 * deletes records from a table
	 * 
	 * @access public 
	 * @param  array
	 * @param  string
	 * @return int 
	 */
	public function delete_records_from_table($table, $where)
	{
		if($this->db->delete($table, $where))
			return  TRUE;
		return FALSE;		
	}  
}
// END Common Model Class 
/* End of file common_model.php */ 
/* Location: ./application/models/common_model.php */