<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersBase extends CI_Model {

	public $variable;
	public $admin_users = "users";
	public function __construct()
	{
		parent::__construct();
		
	}
	/***
	*
	* Display One RECORD in user
	*
	***/
	public function fetchOneAdmin($where='') {
		$this->db->select('A.* ');
	  $this->db->from($this->admin_users." A");
	  if($where!= '') {
			$this->db->where($where);
		}
		$sql = $this->db->get();	
		//ECHO $this->db->last_query();
		$result = array();
		if($sql->num_rows()>0)
		{
			$result = $sql->result();
		}
		return $result[0];
   }

}

/* End of file AdminBase.php */
/* Location: ./application/models/AdminBase.php */