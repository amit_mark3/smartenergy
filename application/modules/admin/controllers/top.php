<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php echo $page_title; ?></title>
<link rel="stylesheet" href="<?php echo $assets_url; ?>/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/main.css">
</head>
<body>