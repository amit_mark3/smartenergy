<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
	private $_controller_url 		= 'admin/login';
	private $_route_url 			= '';
	private $_method_url 			= '';
	private $_sidebar 				= '';  
	private $_form_attributes 		= array(); 

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');  
		$this->load->library('form_validation'); 
		//$this->load->library('recaptcha'); 
		$this->data['msg_success'] = get_success_message($this->session->flashdata('msg_success'));
		$this->data['msg_error'] = get_error_message($this->session->flashdata('msg_error')); 
		//Store the captcha HTML for correct MVC pattern use.
        //$this->data['recaptcha_html'] = $this->recaptcha->get_recaptcha_html(); 
	}

	public function index()
	{
		
		// if already logged in send to dashboard
		if($this->session->userdata('admin_logged_in') === TRUE)
		{
			// redirect to dashboard
			redirect('admin/dashboard');
		}

		/*$this->_breadcrumb_elements = array( 
			array(
				'link' => '#', 
				'link_text' => $this->data['title']
			)
		); 
		
		$this->_method_url = $this->_controller_url . '/news/';
		$this->data['breadcrumb'] =  generate_breadcrumb($this->_breadcrumb_elements, 'news '); */
		/*if(isset($this->session->admin_user_session->id) && $this->session->admin_user_session->id>0)
		{
			redirect('admin/dashboard');
		}*/
		
		$this->_method_url = $this->_controller_url . '/index/'; 
		$this->_route_url = 'login.html';
		$this->data['title'] = 'Login'; 
		
		$this->session->set_flashdata('message', '');
		$this->load->helper(array('form'));
		
		$this->load->helper('security');

		//print_r($_REQUEST);	die;
		$this->load->library('session');
		
		$this->load->model('users');
		//
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('pass', 'pass', 'trim|required');
		//print '<pre>';print_r($this->form_validation);print '</pre>';
		
		if ($this->form_validation->run() == TRUE){
			$username = $this->input->post('username');
			$pass = $this->input->post('pass');
			
			$data = $this->users->check_password($username, $pass);
			
			//print_r($data);die;
			$login_data=$data[0];
			//print_r($data);die;
			if(isset($login_data->id) && $login_data->id>0){
				$this->_set_user_sessions($login_data);
				redirect('admin/dashboard');
			}else{
				$this->session->set_flashdata('message', '<p class="error">'.INVALID_LOGIN.'</p>');
			}
		}else{
			//$this->session->set_flashdata('message', '<p class="error">'.INVALID_LOGIN.'</p>');
			//return false;
		}
		//insert into admin_users (email,username, pass,fullname) values ('admin@christmas.com','Admin', MD5('admin'),'Admin');
		//echo 'Admin Login Screen will come here';

		$this->form_validation->set_error_delimiters('<span class="required ">', '</span>');
		//$params['main_content'] = $this->load->view('loginform', null, TRUE);
		
		//$params['page_title'] = 'Login';
		//$params['assets_url'] = assets_url();/
		//$this->load->view('loginform', $params);
		$this->data['page_title'] 	= 'Login';
		$this->load_admin_view('loginform');
	}

	public function logout(){
		//echo 'dddddd';die;
		$update_data = array('logoutDateTime' => date('Y-m-d H:i:s'));
		$sessionid = $this->session->userdata('sessionid');
		$this->login_model->update_users_ci_logins_log($update_data, $sessionid);
		// destroy login sessions 
		$session_data = array(
			'id'  						=> '',
			'email'     				=> '',
			'first_name' 				=> '',
			'last_name' 				=> '',
			'logo_image' 				=> '',
			'user_type' 				=> '',
			'mobile' 					=> '',
			'created_by' 				=> '',
			'arr_user_roles_id' 		=> '',
			'arr_user_roles_name' 		=> '', 
			'admin_logged_in' 				=> FALSE
		); 
		$this->session->unset_userdata($session_data);     
		$this->session->sess_destroy();
		redirect('login.html');		
	}
	
	/** 
	 * _send_recover_password_email  
	 * 
	 * @access private 
	 * @param array 
	 * @return boolean 
	 */
	private function _send_recover_password_email($data, $new_password)
	{
		$message = "Dear " . $data['first_name'] . ' ' . $data['last_name'] . '<br />Your Login ID is ' . $data['email'] .' and your new password is ' . $new_password . '. Please click <a href="'. site_url('login.html') .'">here</a> to use your new password.';
        // Send an email
        $this->load->library('email'); 
        $this->email->clear();
        $this->email->from($this->config->item('app_from_email'), $this->config->item('app_from_name'));
        $this->email->to($this->config->item('app_alerts_to_email'));
        $this->email->subject('You have requested for a new Password.');
        $this->email->message($message);
        $return['email_send_flag'] = $this->email->send();
		$return['email_send_debug'] = $this->email->print_debugger(); 
		return $return;
	}
	/** 
	 * _set_user_sessions  
	 * Default Method to set the login sessions
	 * 
	 * @access private 
	 * @param  
	 * @return   
	 */
	private function _set_user_sessions($result)
	{ 
		$user_id = $result['0']['id'];
		$session_data['sessionid'] = md5(session_id().time().$user_id);
		$session_data['users_id'] = $user_id;
		// Create login sessions 
		$login_user_data = array(
			'id'  			=> $result['0']['id'],
			'email'     	=> $result['0']['email'],
			'first_name' 	=> $result['0']['first_name'],
			'last_name' 	=> $result['0']['last_name'],
			'user_type' 	=> $result['0']['user_type'],
			'mobile' 	=> $result['0']['mobile'],
			'created_by' => $result['0']['created_by'],
			'sessionid' => $session_data['sessionid'],
			'logo_image' => '',
			'admin_logged_in' 	=> TRUE
		);

		// get the user roles to store in session in array and string format
		//$this->load->model('users/users_model');
		// Get the user assigned roles and save in session array 
		/*$login_user_data['arr_user_roles_id'] 	= array(); // Used for ACL
		$login_user_data['arr_user_roles_name'] = array(); // Used for Navbar
		$arr_user_roles = $this->users_model->get_user_assigned_roles($result['0']['id']);
		if($arr_user_roles !== FALSE)
		{ 
			$login_user_data['arr_user_roles_id'] = array_column($arr_user_roles, 'role_id');
			$login_user_data['arr_user_roles_name'] = array_column($arr_user_roles, 'role_name');
		}*/
		
		
		$this->_set_users_ci_logins_log($session_data);
		$this->session->set_userdata($login_user_data);
		return TRUE;
	}
	private function _set_users_ci_logins_log($session_data){
			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$version = $this->agent->version();
			$platform = $this->agent->platform();
			$mobile = $this->agent->mobile();
			$robot = $this->agent->robot();
			
			$users_id = $session_data['users_id'];
			$sessionid = $session_data['sessionid'];			
			$post_data_users_ci_logins_log = array(
				'users_id'  	=> $users_id,
				'sessionid'  	=> $sessionid,
				'loginDateTime' 	=> date('Y-m-d H:i:s'),	
				'browser' 	=> $browser,
				'browserversion' 	=> $version,
				'platform' 	=> $platform,
				'robot' 	=> $robot,
				'ip' 	=> $_SERVER['REMOTE_ADDR'] ,
				'full_user_agent_string' 	=> $_SERVER['HTTP_USER_AGENT']
			);
			$return = $this->login_model->add_users_ci_logins_log($post_data_users_ci_logins_log);
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */