<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->admin_user_session->id) ){
			redirect('admin/login');
		}
		$this->load->model('settings');
		$this->load->helper(array('sktemplate'));
		$this->load->helper(array('form'));
	}

	public function index()
	{


		//$data['main_content']=$this->load->view('system/general', $data=null, TRUE);
		$data['page_title'] = "General Settings";
		$data['sidebar_html']=$this->load->view('admin/templates/sidebar', null, TRUE);

		$data['listing'] = $this->settings->getGeneralSettings();
		renderTemplate('system/general', 'A', $data);
		//$this->load->view('admin/templates/template', $data);
	}
	public function general($method='edit', $id)
	{
		if(null==$this->input->post())
		{
			if($method == 'edit')
			{
				$setting = $this->settings->getSystemValueRecordByID($id);
				$data['settings_id'] = $setting->id;
				$data['settings_description'] = $setting->description;
				$data['settings_value'] = $setting->value;
				$data['settings_name'] = $setting->name;
				$data['page_title'] = "Settings";
				$data['sidebar_html']=$this->load->view('admin/templates/sidebar', null, TRUE);
				renderTemplate('system/general-settings-edit', 'A', $data);

			}
		}
		else
		{
			if(null!==$this->input->post())
			{
				$action = $this->input->post('action');
				switch ($action)
				{
					case 'save_settings':
					{
						$id = $this->input->post('settings_id', TRUE);
						$value = $this->input->post('settings_value', TRUE);
						$retval = $this->settings->saveSettings($id, $value);
						if($retval==true)
						{
							$this->session->set_flashdata('message', '<p class="success">'."Settings Saved - ".UPDATE_MESSAGE.'</p>');
						}
						else
						{
							$this->session->set_flashdata('message', '<p class="error">'."Settings Not Saved or nothing to save.</p>");
						}
						break;
					}
					default: break;
				}
			redirect('admin/system');
			}
		}
	}
	public function emails($method='list')
	{
		switch ($method)
		{
			case 'list':
			{
				$data['class_general'] = 'active';
				$data['class_email'] = '';
				$data['class_gateway'] = '';
				$data['page_title'] = "Email Settings";
				$data['sidebar_html']=$this->load->view('admin/templates/sidebar', null, TRUE);
				renderTemplate('system/emails', 'A', $data);
				break;
			}
			case 'edit':
			{
				$error = array();
				if( $this->input->post('action') == 'save_settings_name')
				{
					foreach($this->input->post(NULL, TRUE) as $key=>$val)
					{
						switch($key)
						{
							case 'action': break;
							case 'sender_email':
							{
								if(filter_var($val, FILTER_VALIDATE_EMAIL))
								{
									$return = $this->settings->saveSettingsByName($key, $val);
									if(!$return)
									{
										$error[] = 'Sender Email is incorrect or nothing to save.<br>';
									}
								}
								else
								{
									$error[] = 'Sender Email is incorrect or nothing to save.<br>';
								}
								break;
							}
							case 'sender_fromname':
							{
								if(is_string($val) && strlen($val)>0)
								{
									$return = $this->settings->saveSettingsByName($key, $val);
									if(!$return)
									{
										$error[] = 'Sender From Name is incorrect or nothing to save.<br>';
									}
								}
								else
								{
									$error[] = 'Sender From Name is incorrect or nothing to save.<br>';
								}
								break;
							}
							default:{
								if(is_string($val) && strlen($val))
								{
									$return = $this->settings->saveSettingsByName($key, $val);
									if(!$return)
									{
										$error[] = $key.' is incorrect or nothing to save.<br>';
									}
								}
								else
								{
									$error[] = $key. ' is incorrect or nothing to save.<br>';
								}
							}break;
						}
						if(count($error))
						{
							$str = '';
							foreach($error as $error_str)
							{
								$str.='<p class="error">'.$error_str.'</p>';
							}
							$this->session->set_flashdata('message', $str);
						}
						else
						{
							$this->session->set_flashdata('message', '<p class="success"> Settings Saved Successfully!</p>');
						}
					}
				}
				//$data['sidebar_html']=$this->load->view('admin/templates/sidebar', null, TRUE);
				redirect('/admin/system/emails');
				break;
			}
			default: break;
		}
	}

	public function gateways($method='index', $id=null)
	{

		if($method=='index')
		{
			$data['page_title'] = "Gateway Settings";
			$data['listing'] = $this->settings->getGatewaySettings();
			//$this->load->view('admin/templates/template', $data);
			$data['sidebar_html']=$this->load->view('admin/templates/sidebar', null, TRUE);
			renderTemplate('system/gatewaysettings', 'A', $data);
		}
		if($method=='edit')
		{

			    $setting = $this->settings->getSystemValueRecordByID($id);
				$data['settings_id'] = $setting->id;
				$data['settings_description'] = $setting->description;
				$data['settings_value'] = $setting->value;
				$data['settings_name'] = $setting->name;
				$data['page_title'] = "Gateway Settings";
				$data['sidebar_html']=$this->load->view('admin/templates/sidebar', null, TRUE);
				renderTemplate('system/gateway-settings-edit', 'A', $data);
		}

		if($method=='update')
		{
			if(null!==$this->input->post())
			{
				$action = $this->input->post('action');
				switch ($action)
				{
					case 'save_settings':
					{
						$id = $this->input->post('settings_id', TRUE);
						$value = $this->input->post('settings_value', TRUE);
						$type = $this->input->post('type', TRUE);
						$retval = $this->settings->savegatewaysSettings($id, $value,$type);
						if($retval==true)
						{
							$this->session->set_flashdata('message', '<p class="success">'."Settings Saved - ".UPDATE_MESSAGE.'</p>');
						}
						else
						{
							$this->session->set_flashdata('message', '<p class="error">'."Settings Not Saved or nothing to save.</p>");
						}
						break;
					}
					default: break;
				}
				redirect('admin/system/gateways');
			}
		}


	}

}

/* End of file System.php */
/* Location: ./application/controllers/System.php */