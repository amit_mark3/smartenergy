<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->admin_user_session->id)){
			redirect(ADMIN_PATH.'/login/');
		}
	}

	public function index()
	{
		$data['headtitle']='Event';
		$data['breadcrumb'] = set_crumbs(ADMIN_PATH,array(current_url() => $data['headtitle']));	
		//print_r($this->load->model('event'));
		$this->load->model('event');	
		$wh_events['total_rows']='total_rows';
		//$wh_events['where']=' parent_event_id = 0 ';
		$data_event = $this->event->fetchAllEvents($wh_events);
		//print_r($data_event);die;
		$data['data_event'] =$data_event;
		$partial =ADMIN_PATH.'/event/event.php';
		$data['sidebar_html'] = $this->load->view('templates/sidebar',null, true);
		$data['script_load'] = NULL;
		$data['full']=NULL;
		renderTemplate($partial, 'A', $data);
		
	}
	//Add Event using Date Wise
	public function add(){
		if($this->input->post())
		{
		// var_dump($this->input->post());
		// die();
		}
		$this->load->model('event');
		//isset($this->uri->segment(3)) &&
		$event_id = '';
		$category_id = '';
		$frequency ='';
		$offer_selected ='';
		$unique_id=time();
		if( ($this->uri->segment(4) >0)){
			$event_id=$this->uri->segment(4);
			$where = ' event_id = '.$event_id;
			$data_event = $this->event->fetchOneEvents($where);
			$unique_id = $data_event->parent_event_id==0?$event_id:$data_event->parent_event_id;
		}
		//echo $event_id;
		
		$data['headtitle']='Event';
		$data['breadcrumb'] = set_crumbs(ADMIN_PATH,array(current_url() => $data['headtitle']));
		$CI =& get_instance();
		$this->load->library('form_validation');
		$this->load->helper(array('form'));
		$this->load->helper('security');
		$login_admin_id =trim($this->session->admin_user_session->id);
		
		$recurrence_bydays='';
		$recurrence_byweekno='';
			
		$this->form_validation->set_rules('event_title', 'Title', 'trim|required|max_length[50]');
		//$this->form_validation->set_rules('category_id', 'Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|max_length[50]|callback_compareDate');
		//$this->form_validation->set_rules('recurrence_freq', 'Frequency', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('start_time', 'Start Time', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('end_time', 'End Time', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('evt_total_spaces', 'Total Spaces', 'trim|required|max_length[50]');
		$category_id = $this->input->post('category_id');
		$recurrence_freq = $this->input->post('recurrence_freq');
		if($this->form_validation->run() == TRUE)
        {
			
			$event_data = $this->input->post();
			//var_dump($event_data);die();
			extract($event_data);
			$start_date =date('Y-m-d',strtotime($start_date));
			$end_date =date('Y-m-d',strtotime($end_date));
			//echo $wheelchair;die;
			//print_r($recurrence_bydays);die;
			$saveupdt_event_data = array('event_title'=>$event_title,
										 'category_id'=>$category_id,
										 'start_date'=>$start_date,
										 'end_date'=>$end_date,
										 'start_time'=>$start_time,
										 'end_time'=>$end_time,
										 'evt_total_spaces'=>$evt_total_spaces										 
									 );
			if($event_id==''){
				//print 'ffffffff'; print_r($update_data);  die;
				$event_id = $this->event->saveEvents($saveupdt_event_data);
				if($name!=''){
					$ticket_types = isset($ticket_type)?$ticket_type:2;	
					$saveupdt_ticket_data = array('name'=>$name,
											 'event_id'=>$unique_id,
											 'price'=>$price,
											 'total_spaces'=>$total_spaces,
											 'ticket_type'=>$ticket_types
											 
										 );
					$ticket_id = $this->event->saveEventsTicket($saveupdt_ticket_data);
				}
				if($event_id>0){
					$updt_eventticket_data = array('event_id'=>$event_id);
					$eret_val = $this->event->updateEventsTicketsIsEventId($updt_eventticket_data,$unique_id);
					$event_frequency_data['start_date']=$start_date;
					$event_frequency_data['end_date'] = $end_date;
					$event_frequency_data['frequency_interval'] = $frequency_interval==''?1:$frequency_interval;					
					$recurrencebydays='';
					if(isset($recurrence_bydays) && $recurrence_bydays!=''){
						$recurrencebydays=  implode(",", $recurrence_bydays);
					}	
					
					$event_frequency_data['recurrence_bydays']= $recurrencebydays;
					$event_frequency_data['recurrence_byweekno']=$recurrence_byweekno;
					$event_frequency_data['recurrence_freq']=$recurrence_freq;
					$inc_start_date='';
					$savemul_event_data=array();
					
					$date_data=$this->get_recurrence_days($event_frequency_data);
					//print_r($date_data);die;
					if(count($date_data)>0){
						for($i=0;$i<count($date_data);$i++){
							$inc_start_date = date('Y-m-d',$date_data[$i]);
							//$savemul_event_data[$i]
							$savemul_event_data = array('event_title'=>$event_title,
																 'category_id'=>$category_id,
																 'parent_event_id'=>$event_id,
																 'start_date'=>$inc_start_date,
																 'end_date'=>$inc_start_date,
																 'start_time'=>$start_time,
																 'end_time'=>$end_time,
																 'evt_total_spaces'=>$evt_total_spaces		
															 );
							$new_event_id = $this->event->saveEvents($savemul_event_data);	
							if($new_event_id>0){
								$where_tick['total_rows'] ='total_rows';
								$where_tick['where'] =' event_id ='.$event_id.' ';
								$ticketData = $this->event->fetchAllEventsTickets($where_tick);
								$ticket_row =$ticketData['row'];
								$html_data ='';
								if($ticketData['total_rows']>0){
									foreach($ticket_row as $k=>$val){
										$ticket_type = $val->ticket_type;
										$ticket_types = isset($ticket_type)?$ticket_type:2;	
										$save_ticket_data = array('name'=>$val->name,
											 'event_id'=>$new_event_id,
											 'price'=>$val->price,
											 'total_spaces'=>$val->total_spaces,
											 'ticket_type'=>$ticket_types
										 );
										$ticket_id = $this->event->saveEventsTicket($save_ticket_data);
									}
								}
							
							//Kunal for Offer Master coding//
							if(isset($offer_selected) && count($offer_selected)>0){
								foreach($offer_selected as $k=>$offer_id){									
									$offer_arr = array('offer_id'=>$offer_id, 'event_id'=>$new_event_id);
									$this->event->saveEventsOffers($offer_arr);										
								}
							}
							//Kunal for Offer Master coding - End//
							}
							
							
						}
						//print'<pre>';print_r($savemul_event_data);print'</pre>';die;
						//$this->event->saveEventsBatch($savemul_event_data);
					}
					// delete main event id in  events tickets and event 
					$this->event->deleteEventsTicketEventId($event_id);
					$this->event->deleteEvents($event_id);	
					$evtupdate_data	= array('parent_event_id'=>'0');				
					$this->event->updateEventsParentId($evtupdate_data,$event_id);					
					//print_r($estart_date);
					$this->session->set_flashdata('message', '<p class="success">Event '.ADD_MESSAGE.'</p>');
					redirect(ADMIN_PATH.'/events/');
				}else{
					$this->session->set_flashdata('message', '<p class="error">Event '.ERROR_MESSAGE.'</p>');

				}
			}else{				
				$unique_id =$event_id;	
				if($name!=''){	
					$ticket_types = isset($ticket_type)?$ticket_type:2;	
					$saveupdt_ticket_data = array('name'=>$name,
											 'event_id'=>$unique_id,
											 'price'=>$price,
											 'total_spaces'=>$total_spaces,
											 'ticket_type'=>$ticket_types	
										 );
					$ticket_id = $this->event->saveEventsTicket($saveupdt_ticket_data);
				}
				
				$ret_val = $this->event->updateEvents($saveupdt_event_data,$event_id);
				if($ret_val>0){
					$this->session->set_flashdata('message', '<p class="success">Event '.UPDATE_MESSAGE.'</p>');
					redirect(ADMIN_PATH.'/events/');
				}else{
					$this->session->set_flashdata('message', '<p class="error">Event '.ERROR_MESSAGE.'</p>');
				}
			}
			

		}else{
			$this->session->set_flashdata('message','');
			if(isset($event_id) && $event_id>0){
				$where = ' event_id = '.$event_id;
				$data_event = $this->event->fetchOneEvents($where);
				//print_r($data_event);
				if(isset($data_event->event_id)){
					$category_id = $data_event->category_id;
					$frequency = $data_event->frequency;
					$data['data_event'] =$data_event;
				}else{
					$category_id ='';
					$data['data_event'] ='';
				}
				//print_r($data['data_event']->);
			}
			//print_r($user->id);
		}
		
		//saveEventsBatch
		//print_r($estart_date);
		$where_tick['where'] =" event_id = '".$unique_id."'";
		$data['ticket_data'] = $this->displayEventData($where_tick);
		$data_event_ticket	='';	
		$data['unique_id'] =$unique_id;	
		$data['data_event_ticket'] =$data_event_ticket;		
		$data['optionFrequency'] =array('daily'=>'Daily','weekly'=>'Weekly','monthly'=>'Monthly','yearly'=>'Yearly');//
		//echo $category_id; 25/05/2016
		$data['event_id'] =$event_id;
		$data['category_id'] =$category_id;
		$data['recurrence_freq'] =$recurrence_freq;
		$opt_where = ' status=1 ';
		$data['optionCategory'] =$this->event->optionEventCategory($opt_where);
		//print_r($data['option_category']);
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$partial =ADMIN_PATH.'/event/event_ae.php';
		$this->load->model('offer');
		$data['offers']  = $this->offer->getOffers($event_id);
		$data['offers_selected']  = $this->offer->getOffersSelected($event_id);


		$data['sidebar_html'] = $this->load->view('templates/sidebar',null, true);
		$data['script_load'] = array(base_url().'assets/js/admin.js',base_url().'assets/js/bootstrap-datepicker.js',base_url().'assets/vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js');
		$data['full']=NULL;
		renderTemplate($partial, 'A', $data);		
	}
	// for compare start date and end date
	function compareDate() {
		  $startDate = strtotime($this->input->post('start_date'));
		  $endDate = strtotime($this->input->post('end_date'));
		  if ($endDate >= $startDate)
			return True;
		  else {
			$this->form_validation->set_message('compareDate', '%s should be less than Start Date.');
			return False;
		  }
	}
	// For adding Ticket via ajax
	function ajaxEventTicket(){
		$ticket_id='';
		$this->load->model('event');
		$CI =& get_instance();
		$this->load->library('form_validation');
		$this->load->helper(array('form'));
		$this->load->helper('security');
		$login_admin_id =trim($this->session->admin_user_session->id);		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		if($this->form_validation->run() == TRUE)
        {
			$ticket_data = $this->input->post();
			//print_r($ticket_data);
			extract($ticket_data);
			//echo $wheelchair;die;		
				$ticket_types = isset($ticket_type)?$ticket_type:2;	
			$saveupdt_ticket_data = array('name'=>$name,
										 'event_id'=>$unique_id,
										 'price'=>$price,
										 'total_spaces'=>$total_spaces,
										 'ticket_type'=>$ticket_types										 
									 );
			if($ticket_id==''){
				$ticket_id = $this->event->saveEventsTicket($saveupdt_ticket_data);
				if(isset($ticket_id) && $ticket_id>0){
					$where_tick['where'] =" event_id = '".$unique_id."'";
					$event_html_data = $this->displayEventData($where_tick);
					$ajax_status ='true';
					$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id,"event_ticket_data"=>$event_html_data);					
				}else{
					$ajax_status='false';
					$message='false';
					$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id,"event_ticket_data"=>$message);
				}
				echo json_encode($DataItem);
				//echo $html_data;
			}else{	
				$ret_val = $this->event->updateEventsTickets($saveupdt_ticket_data,$ticket_id);				
				if($ret_val>0){
					$ajax_status ='true';
					$where_tick['where'] =" event_id = '".$unique_id."'";
					$event_html_data = $this->displayEventData($where_tick);
					$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id,"event_ticket_data"=>$event_html_data);	
				}else{
					$ajax_status='false';
					$message =$this->session->set_flashdata('message', '<p class="error">Event '.ERROR_MESSAGE.'</p>');
					$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id,"event_ticket_data"=>$message);
				}
				echo json_encode($DataItem);				
			}	
		}
	}
	function displayEventData($where_tick){
		$where_tick['total_rows'] ='total_rows';
		$DataItem = $this->event->fetchAllEventsTickets($where_tick);
		$data_row =$DataItem['row'];
		//print_r($DataItem);die;
		$html_data ='';
		if($DataItem['total_rows']>0){
			$ticket_type = '';
			foreach($data_row as $k=>$val){
				$price = money_format('%n', $val->price);	
				if($val->ticket_type==1){$ticket_type ='Adult';}elseif($val->ticket_type==2){$ticket_type ='Child';}else{{$ticket_type ='Baby';}}
				$html_data.="<tr id='ticket_id_".$val->ticket_id."' class='trshowhide'><td>".$val->name."</td><td>".$price."</td><td>".$val->total_spaces."</td><td>".$ticket_type."</td><td><a href='javascript:void(0);' id='edit_ticket' onclick='edit_ticket(".$val->ticket_id.");'><span class='glyphicon glyphicon-edit'></span></a>&nbsp;|&nbsp;<a href='javascript:void(0);' onclick='delete_ticket(".$val->ticket_id.")'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
			}
		}
		$event_html_data="<table class='table' width='100%'><tr><th>Name</th><th>Price</th><th>Spaces</th><th>Ticket Type</th><th>Action</th>".$html_data."</table>";
		return $event_html_data;
	}
	// For Fetch Ticket Data for Edit
	function ajaxGetEventTicket(){
		$this->load->model('event');
		$ticket_id ='';
		$ticket_data = $this->input->post();
		extract($ticket_data);
		if($ticket_id>0){
			$where =" ticket_id = '".$ticket_id."'";
			$event_ticket_data = $this->event->fetchOneEventsTicket($where);
			//print_r($event_ticket_data);
			$ajax_status ='true';
			$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id,"event_ticket_data"=>$event_ticket_data);
		}else{
			$ajax_status ='false';
			$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id);
		}
		echo json_encode($DataItem);	
		
	}	
	// For Fetch Ticket Data for Edit
	function ajaxDelEventTicket(){
		$this->load->model('event');
		$ticket_id ='';
		$ticket_data = $this->input->post();
		extract($ticket_data);
		if($ticket_id>0){
			$event_ticket_data = $this->event->deleteEventsTicket($ticket_id);
			//print_r($event_ticket_data);
			if($event_ticket_data>0){
				$ajax_status ='true';
				$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id,"event_ticket_data"=>$event_ticket_data);
			}
		}else{
			$ajax_status ='false';
			$DataItem = array("ajax_status"=>$ajax_status,"ticket_id"=>$ticket_id);
		}
		echo json_encode($DataItem);	
		
	}
	// Return data For date wise Frequency daily, weekly monthly in event
	function get_recurrence_days($event_frequency_data){
		
			$this->event_start_date =$event_frequency_data['start_date'];//start_date
			$this->event_end_date =$event_frequency_data['end_date'];//end_date
			$this->recurrence_interval =$event_frequency_data['frequency_interval'];//frequency_interval
			$this->recurrence_byday =$event_frequency_data['recurrence_bydays'];//recurrence_bydays
			$this->recurrence_byweekno =$event_frequency_data['recurrence_byweekno'];
			$this->recurrence_freq =$event_frequency_data['recurrence_freq'];//frequency
								
			
		//event_start_date,event_end_date,recurrence_interval,recurrence_byday recurrence_byweekno  recurrence_freq  
		$start_date = strtotime($this->event_start_date); 
		$end_date = strtotime($this->event_end_date);
				
		$weekdays = explode(",", $this->recurrence_byday); //what days of the week (or if monthly, one value at index 0)
		// $weekdays = $this->recurrence_byday;
		$matching_days = array(); 
		$aDay = 86400;  // a day in seconds
		$aWeek = $aDay * 7;		 
			
		//TODO can this be optimized?
		switch ( $this->recurrence_freq ){
			case 'daily':
				//If daily, it's simple. Get start date, add interval timestamps to that and create matching day for each interval until end date.
				$current_date = $start_date;
				while( $current_date <= $end_date ){
					$matching_days[] = $current_date;
					$current_date = $current_date + ($aDay * $this->recurrence_interval);
				}
				break;
			case 'weekly':
				//sort out week one, get starting days and then days that match time span of event (i.e. remove past events in week 1)
				$start_of_week = 0;//get_option('start_of_week'); //Start of week depends on WordPress
				//first, get the start of this week as timestamp
				$event_start_day = date('w', $start_date);
				//then get the timestamps of weekdays during this first week, regardless if within event range
				$start_weekday_dates = array(); //Days in week 1 where there would events, regardless of event date range
				for($i = 0; $i < 7; $i++){
					$weekday_date = $start_date+($aDay*$i); //the date of the weekday we're currently checking
					$weekday_day = date('w',$weekday_date); //the day of the week we're checking, taking into account wp start of week setting

					if( in_array( $weekday_day, $weekdays) ){
						$start_weekday_dates[] = $weekday_date; //it's in our starting week day, so add it
					}
				}					
				//for each day of eventful days in week 1, add 7 days * weekly intervals
				foreach ($start_weekday_dates as $weekday_date){
					//Loop weeks by interval until we reach or surpass end date
					while($weekday_date <= $end_date){
						if( $weekday_date >= $start_date && $weekday_date <= $end_date ){
							$matching_days[] = $weekday_date;
						}
						$weekday_date = $weekday_date + ($aWeek *  $this->recurrence_interval);
					}
				}//done!
				break;  
			case 'monthly':
				//loop months starting this month by intervals
				$current_arr = getdate($start_date);
				$end_arr = getdate($end_date);
				$end_month_date = strtotime( date('Y-m-t', $end_date) ); //End date on last day of month
				$current_date = strtotime( date('Y-m-1', $start_date) ); //Start date on first day of month
				while( $current_date <= $end_month_date ){
					$last_day_of_month = date('t', $current_date);
					//Now find which day we're talking about
					$current_week_day = date('w',$current_date);
					$matching_month_days = array();
					//Loop through days of this years month and save matching days to temp array
					for($day = 1; $day <= $last_day_of_month; $day++){
						if((int) $current_week_day == $this->recurrence_byday){
							$matching_month_days[] = $day;
						}
						$current_week_day = ($current_week_day < 6) ? $current_week_day+1 : 0;							
					}
					//Now grab from the array the x day of the month
					$matching_day = ($this->recurrence_byweekno > 0) ? $matching_month_days[$this->recurrence_byweekno-1] : array_pop($matching_month_days);
					$matching_date = strtotime(date('Y-m',$current_date).'-'.$matching_day);
					if($matching_date >= $start_date && $matching_date <= $end_date){
						$matching_days[] = $matching_date;
					}
					//add the number of days in this month to make start of next month
					$current_arr['mon'] += $this->recurrence_interval;
					if($current_arr['mon'] > 12){
						//FIXME this won't work if interval is more than 12
						$current_arr['mon'] = $current_arr['mon'] - 12;
						$current_arr['year']++;
					}
					$current_date = strtotime("{$current_arr['year']}-{$current_arr['mon']}-1"); 
				}
				break;
			case 'yearly':
				//If yearly, it's simple. Get start date, add interval timestamps to that and create matching day for each interval until end date.
				//echo $event_frequency_data['start_date']; die;
				$month = date('m', strtotime($event_frequency_data['start_date']));
				$day = date('d',strtotime($event_frequency_data['start_date']));
				$year = date('Y',strtotime($event_frequency_data['start_date']));
				$end_year = date('Y',strtotime($event_frequency_data['end_date'])); 
				if( @mktime(0,0,0,$day,$month,$end_year) < $event_frequency_data['end_date'] ) $end_year--;
				while( $year <= $end_year ){
					$matching_days[] = mktime(0,0,0,$month,$day,$year);
					$year++;
				}			
				break;
		}
		//print_r($matching_days);die;
		sort($matching_days);
		return $matching_days;
		//print '<pre>';print_r($matching_days);print'</pre>';die;
		//return apply_filters('em_events_get_recurrence_days', $matching_days, $this);
	}
	

}

/* End of file Event.php */
/* Location: ./application/controllers/Event.php */