<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->admin_user_session->id))
		{
			redirect(ADMIN_PATH.'/login/');
		}
	}
	public function index()
	{
		$data['headtitle']='';
		$data['breadcrumb'] = set_crumbs(ADMIN_PATH,array(current_url() => $data['headtitle']));
		$this->load->library('form_validation');
		$partial =ADMIN_PATH.'/dashboard.php';
		$data['script_load'] = null;
		$data['sidebar_html'] = $this->load->view('templates/sidebar',null, true);
		renderTemplate($partial, 'A', $data);
		//echo 'dashboard<br/><br/><a href="login/logout/" >Logout</a>';
	}


}
