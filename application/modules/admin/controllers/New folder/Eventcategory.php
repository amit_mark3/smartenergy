<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventcategory extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->admin_user_session->id)){
			redirect(ADMIN_PATH.'/login/');
		}
	}

	public function index()
	{
		$data['headtitle']='Event Category';
		$data['breadcrumb'] = set_crumbs(ADMIN_PATH,array(current_url() => $data['headtitle']));
		$this->load->model('event');
		$wh_ec['total_rows']='total_rows';
		$data_eventcategory = $this->event->fetchAllEventCategory($wh_ec);
		$data['data_eventcategory'] =$data_eventcategory;
		$partial =ADMIN_PATH.'/eventcategory/eventcategory.php';
		$data['sidebar_html'] = $this->load->view('templates/sidebar',null, true);
		$data['script_load'] = NULL;
		$data['full']=NULL;
		renderTemplate($partial, 'A', $data);
	}

	public function add(){
		$this->load->model('event');
		//isset($this->uri->segment(3)) &&
		$category_id = '';
		if( ($this->uri->segment(4) >0)){
			$category_id=$this->uri->segment(4);
		}
		//echo $category_id;

		$data['headtitle']='Event Category';
		$data['breadcrumb'] = set_crumbs(ADMIN_PATH,array(current_url() => $data['headtitle']));
		$CI =& get_instance();
		$this->load->library('form_validation');
		$this->load->helper(array('form'));
		$this->load->helper('security');
		$login_admin_id =trim($this->session->admin_user_session->id);

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('color', 'Event Calendar Color', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('category_content', 'Event Category Contents', 'trim|required|min_length[10]');
		$this->form_validation->set_rules('category_short_content', 'Event Category Contents', 'trim|required|min_length[10]');
		$this->form_validation->set_rules('category_terms', 'Event Terms and Conditions', 'trim|required|min_length[10]');
		if($this->form_validation->run() == TRUE)
        {
			$cat_event_data = $this->input->post();
			extract($cat_event_data);
			//echo $wheelchair;die;

			$saveupdt_cat_event_data = array('name'=>$name, 'color'=>$color, 'content'=>$category_content, 'short_content'=>$category_short_content,  'terms'=>$category_terms);
			if($category_id==''){
				//print 'ffffffff'; print_r($update_data);  die;
				$category_id = $this->event->saveEventCategory($saveupdt_cat_event_data);
				if($category_id>0){
					$this->session->set_userdata('category_id',$category_id);
					//echo $this->session->category_id;
					//$this->session->set_flashdata('message', '<p class="success">'.ADD_MESSAGE.'</p>');
					redirect(ADMIN_PATH.'/eventcategory/');
				}else{
					$this->session->set_flashdata('message', '<p class="error">'.ERROR_MESSAGE.'</p>');

				}
			}else{
				$ret_val = $this->event->updateEventCategory($saveupdt_cat_event_data,$category_id);
				if($ret_val>0){
					//$this->session->set_flashdata('message', '<p class="success">'.UPDATE_MESSAGE.'</p>');
					redirect(ADMIN_PATH.'/eventcategory/');

				}else{
					$this->session->set_flashdata('message', '<p class="error">'.ERROR_MESSAGE.'</p>');

				}
			}

		}else{
			$this->session->set_flashdata('message','');
			if(isset($category_id) && $category_id>0){
				$where = ' category_id = '.$category_id;
				$data_event_category = $this->event->fetchOneEventCategory($where);
				if(isset($data_event_category->category_id)){
					$category_id = $data_event_category->category_id;
					$data['event_category'] =$data_event_category;
				}else{
					$category_id ='';
					$data['event_category'] ='';
				}
				//print_r($data['event_category']->);
			}
			//print_r($user->id);
		}
		//echo $category_id;

		$data['category_id'] =$category_id;
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$partial =ADMIN_PATH.'/eventcategory/eventcategory_ae.php';
		$data['sidebar_html'] = $this->load->view('templates/sidebar',null, true);
		$data['script_load'] = null;
		$data['full']=NULL;
		renderTemplate($partial, 'A', $data);

	}
	function statusUpdate(){
		$this->load->model('event');
		if( ($this->uri->segment(4) >0)){
			$category_id=$this->uri->segment(4);
			$wh_ec=' category_id='.$category_id;
			$data_eventcategory = $this->event->fetchOneEventCategory($wh_ec);
			$status =$data_eventcategory->status;
			$status =$status==1?0:1;
			$saveupdt_cat_event_data = array('status'=>$status);
			$ret_val = $this->event->updateEventCategory($saveupdt_cat_event_data,$category_id);
		}
		redirect(ADMIN_PATH.'/eventcategory/');

	}

}

/* End of file Eventcategory.php */
/* Location: ./application/controllers/Eventcategory.php */