<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php echo $page_title; ?></title>
<link rel="stylesheet" href="<?php echo $assets_url; ?>/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/main.css">
</head>
<body>

    <div class="page-center" style="background-image:url('<?php echo $assets_url; ?>/img/login.jpg'); background-size:cover;">
        <div class="page-center-in">
        
            <div class="container-fluid" >
            <img src="<?php echo $assets_url; ?>/img/logo16.png" class=" img-responsive center-block" style="margin-left:38.3%; margin-bottom:20px;" width="290px">
                <form class="sign-box" style="color:#fff;" method="post">                 
                    <div class="sign-avatar">
                        <img src="<?php echo $assets_url; ?>/img/avatar-sign.png" alt="">                    </div>
                    <header class="sign-title">Login</header>
					<?php echo $this->session->flashdata('message'); ?>
                    <div class="form-group" >
						<span class="form_login"><?php echo form_error('username'); ?>&nbsp;</span>
							<?php echo form_input(array('id' => 'username', 'name' => 'username','class'=>'form-control', 'placeholder'=>'E-Mail or Phone', 'value' => set_value('username'))); ?>
                    </div>
                    <div class="form-group">
						<span class="form_login"><?php echo form_error('pass'); ?>&nbsp;</span>
							<?php echo form_password(array('id' => 'pass', 'name' => 'pass','class'=>'form-control', 'placeholder'=>'Password', 'value' => set_value('pass'))); ?>
                    </div>
                    <div class="form-group">
                        <div>
                            <input type="checkbox" id="signed-in"/> Keep me signed in
                        </div>
                        <div class="float-right reset">
                            <a href="reset-password.html" style="color:#fff;">Reset Password</a>                        </div>
                    </div>
                        
                    <button type="submit" class="btn btn-rounded" style="background-color:rgba(0, 168, 255, 0); border:1px solid #fff;"><a href="index.html" style="color:white; ">Login</a></button>
                   <!-- <p class="sign-note">New to our website? <a href="sign-up.html">Sign up</a></p>-->
                    <!--<button type="button" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>-->
                </form>
            </div>
        </div>
    </div><!--.page-center-->
<script src="<?php echo $assets_url; ?>/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo $assets_url; ?>/js/lib/tether/tether.min.js"></script>
<script src="<?php echo $assets_url; ?>/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo $assets_url; ?>/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo $assets_url; ?>/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="<?php echo $assets_url; ?>/js/app.js"></script>
</body>
</html>
