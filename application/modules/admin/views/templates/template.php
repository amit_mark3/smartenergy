<!DOCTYPE html>
<html>
  <head>
    <title>Smart Energy - <?php echo isset($page_title)?$page_title:'';?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet">

	
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<script>const SITE_URL ='<?php echo site_url();?>';const ADMIN_PATH ='<?php echo ADMIN_PATH;?>';</script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Christmasisland.ie Admin</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form">
	                       <input type="text" class="form-control" placeholder="Search...">
	                       <span class="input-group-btn">
	                         <button class="btn btn-primary" type="button">Search</button>
	                       </span>
	                  </div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.html">Profile</a></li>
	                          <li><a href="<?php echo site_url(ADMIN_PATH. '/login/logout/'); ?>">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<!-- Start Side Bar -->
				<?php echo isset($sidebar_html)?$sidebar_html:'&nbsp;';?>
		  </div>
		  <div class="col-md-10">
		  	<div class="breadcrumb" id ="breadcrumb"><?php echo isset($breadcrumb) ? $breadcrumb : ''; ?></div>
		  	<div class="content-box-large box-with-header">
				<div class="content-box-header">
			  					<div class="panel-title"><?php echo isset($page_title)?$page_title:'&nbsp;';?></div>
	  			</div>
				<div id="message_sh">
						<?php echo $this->session->flashdata('message'); ?>
						<?php if(!isset($login_page)) echo validation_errors(); ?>
				</div>
				<?php echo isset($main_content)?$main_content:'&nbsp;'?>
		  	</div>
		  </div>
		</div>
    </div>

<?php echo $footer_html; ?>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    
	<?php echo $script_load; ?>
	 <!-- bootstrap-datetimepicker -->
    
  </body>
</html>