<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.html"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Event Category
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="<?php echo site_url(ADMIN_PATH. '/eventcategory/add/'); ?>">Add</a></li>
                            <li><a href="<?php echo site_url(ADMIN_PATH. '/eventcategory/'); ?>">List</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Events
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
							<li><a href="<?php echo site_url(ADMIN_PATH. '/events/add/'); ?>">Add</a></li>
                            <li><a href="<?php echo site_url(ADMIN_PATH. '/events/'); ?>">List</a></li>
                        </ul>
                    </li>

					<li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i>Offers
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
							<li><a href="<?php echo site_url(ADMIN_PATH. '/offer/add/'); ?>">Add</a></li>
                            <li><a href="<?php echo site_url(ADMIN_PATH. '/offer/'); ?>">List</a></li>
                        </ul>
                    </li>

                    <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Bookings</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Transactions</a></li>

                    <li class="submenu">
                         <a href="/admin/system">
                            <i class="glyphicon glyphicon-list"></i> Settings
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="/admin/system">General</a></li>
                            <li><a href="/admin/system/emails">Emails</a></li>
                            <li><a href="/admin/system/gateways">Gateway Settings</a></li>

                        </ul>
                    </li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Reports
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="login.html">Check In Sheet</a></li>
                            <li><a href="signup.html">Santa</a></li>
                            <li><a href="signup.html">Elf</a></li>
                            <li><a href="signup.html">Cancellation Info</a></li>
                            <li><a href="signup.html">Donation Info</a></li>

                        </ul>
                    </li>
                </ul>
             </div>