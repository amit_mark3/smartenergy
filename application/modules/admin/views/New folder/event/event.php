<div class="row">
<div class="col-md-12">
	<div class="table-responsive">
		<table class="table">
		  <thead>
			<tr>
			  <th>Title</th>
			  <th>Category</th>
			  <th>Start Date</th>
			  <th>End Date</th>
			  <th>Start Time</th>
			  <th>End Time</th>
			  <th>Action</th>
			</tr>
		  </thead>
		  <tbody>
		   <?php 
			if($data_event['total_rows']>0){
				$row = $data_event['row'];	
				foreach($row as $event){
					$event_id = trim($event->event_id);
					$category_name = stripslashes($event->category_name);					
					$event_title = stripslashes($event->event_title);
					$start_date = (date('Y-m-d',strtotime($event->start_date)));
					$end_date = (date('Y-m-d',strtotime($event->end_date)));
					$start_time = (date('H:i',strtotime($event->start_time)));
					$end_time = (date('H:i',strtotime($event->end_time)));
					//$event_status = $event->status==1?'Active':'Inactive';
			?>	
			<tr id="event_category_<?php echo $event_id; ?>">
			    <td><?php echo $event_title; ?></td>
				 <td><?php echo $category_name; ?></td>
				<td><?php echo $start_date; ?></td>
				<td><?php echo $end_date; ?></td>
				<td><?php echo $start_time; ?></td>
				<td><?php echo $end_time; ?></td>
				<td><a href="<?php echo site_url(ADMIN_PATH. '/events/add/'.$event_id); ?>">Edit</a></td>
			</tr>	
			<?php 
					}
				}else{
					echo '<tr><td colspan="3">'.NO_RESULT_FOUND.'</td></tr>';	
				}
			?>
			</tbody>
		</table>		
	</div>					  
</div>
</div>
	
