<div class="row">
<div class="col-md-12">
	<div class="content-box-large box-with-header">
		<?php echo form_open(NULL, 'id="form_event"', 'class="form-inline"'); ?>
			<fieldset>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<?php echo form_label('Title', 'event_title', array('class' => 'control-label'))?>
						<?php echo form_input(array('id' => 'event_title', 'name' => 'event_title','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('event_title', (isset($data_event->event_title)) ? $data_event->event_title : ''))); ?>    				
					</div>
					<div class="form-group">
						<?php echo form_label('Total Spaces', 'evt_total_spaces', array('class' => 'control-label'))?>
						<?php echo form_input(array('id' => 'evt_total_spaces', 'name' => 'evt_total_spaces','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('evt_total_spaces', (isset($data_event->evt_total_spaces)) ? $data_event->evt_total_spaces : ''))); ?>    				
					</div>

				</div>
				<div class="col-md-4">
					
					<div class="form-group">
					<?php if(!empty($optionCategory)){?>
						<?php echo form_label('Category', 'category_id', array('class' => 'control-label'))?>
						<?php echo form_dropdown('category_id', array('' => '--Select--') + $optionCategory,$category_id ,'class="form-control"');?>
					<?php }?>		
					</div>
					
					
					
				</div>
				
			
			
			</div>
			<?php if($event_id=='') { ?>
			<div class ="row">
				<div class="col-md-10">
					
					<?php if(!empty($optionFrequency)){?>
						<?php echo form_label('Frequency', 'recurrence_freq', array('class' => 'control-label'))?>
					<div class="row">
						<div class="col-sm-2">
							<?php echo form_dropdown('recurrence_freq', $optionFrequency,$recurrence_freq ,'class="form-control" id="recurrence_freq"');?>
						</div>
						<div class="col-sm-1">Every</div>
						<div class="col-sm-1">
							<?php echo form_input(array('id' => 'frequency_interval', 'name' => 'frequency_interval','class'=>'form-control', 'type' => 'text',  'maxlength'=>'2', 'value' => set_value('frequency_interval', (isset($data_event->frequency_interval)) ? $data_event->frequency_interval : ''))); ?>						
						</div>
						<div class="col-sm-1"><span id="frequency_dwmy">Day(s)</span></div>
						<div id="monthly-selector" style="display:none;">
							<div class="col-sm-2" >
								<select name="recurrence_byweekno" id="monthly-modifier" class="form-control">
									<option selected="selected" value="1">first</option>
									<option value="2">second</option>
									<option value="3">third</option>
									<option value="4">fourth</option>
									<option value="-1">last</option>
								</select>
							</div>	
							<div class="col-sm-2" >
								<select name="recurrence_byday" id="recurrence-weekday" class="form-control">
									<option value="1">Mon</option>
									<option value="2">Tue</option>
									<option value="3">Wed</option>
									<option value="4">Thu</option>
									<option value="5">Fri</option>
									<option value="6">Sat</option>
									<option selected="selected" value="0">Sun</option>
								</select>
								
							</div>
							<div class="col-sm-1" >of each month</div>
						</div>
	
					</div>
					<div class ="row">
				<div class="col-md-10">
					<div id="weekly-selector" class="alternate-selector" style="display: none;">
		<input type="checkbox" value="1" name="recurrence_bydays[]"> Mon&nbsp; <input type="checkbox" value="2" name="recurrence_bydays[]"> Tue&nbsp; <input type="checkbox" value="3" name="recurrence_bydays[]"> Wed&nbsp; <input type="checkbox" value="4" name="recurrence_bydays[]"> Thu&nbsp; <input type="checkbox" value="5" name="recurrence_bydays[]"> Fri&nbsp; <input type="checkbox" value="6" name="recurrence_bydays[]"> Sat&nbsp; <input type="checkbox" value="0" name="recurrence_bydays[]"> Sun&nbsp; 	</div>
		</div></div>
		

		
						
					
					<?php }?>		
					
					
				</div>
			</div>
			<?php } ?>
			<div class="row"><div class="col-md-10">&nbsp;</div></div>
		<div class="row">
		<div class="col-md-4">
					<div class="form-group">
						<?php echo form_label('Events Start From Date', 'start_date', array('class' => 'control-label'))?>
						<?php echo form_input(array('id' => 'start_date', 'name' => 'start_date','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('start_date', (isset($data_event->start_date)) ? date('d-m-Y',strtotime($data_event->start_date)) : ''))); ?>    				
					</div>
					<div class="form-group">
						<?php echo form_label('Events Start From Time', 'start_time', array('class' => 'control-label'))?>
						<?php echo form_input(array('id' => 'start_time', 'name' => 'start_time','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('start_time', (isset($data_event->start_time)) ? date('H:i',strtotime($data_event->start_time)) : ''))); ?>    				
					</div>
		</div>			
		<div class="col-md-4">			
					
					
					<div class="form-group">
						<?php echo form_label('To Date', 'end_date', array('class' => 'control-label'))?>
						<?php echo form_input(array('id' => 'end_date', 'name' => 'end_date','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('end_date', (isset($data_event->end_date)) ? date('d-m-Y',strtotime($data_event->end_date)) : ''))); ?>    				
					</div>
					<div class="form-group">
						<?php echo form_label('To Time', 'end_time', array('class' => 'control-label'))?>
						<?php echo form_input(array('id' => 'end_time', 'name' => 'end_time','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('end_time', (isset($data_event->end_time)) ? date('H:i',strtotime($data_event->end_time)) : ''))); ?>    				
					</div>
					
		</div>
		</div>
			<div class="row"><br><br><div class="col-md-10"><h3>Add Tickets</h3></div></div>
			<div class="row"><div class="col-md-10">
			<?php //<!-- Start for Ticket Data --->?>
			<div class="row"><div class="col-md-10"><div id="ticket_data"><?php echo $ticket_data; ?> </div></div></div>
			<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<?php echo form_label('Name', 'name', array('class' => 'control-label'))?>
					<?php echo form_input(array('id' => 'name', 'name' => 'name','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('name', (isset($data_event_ticket->name)) ? $data_event_ticket->name : ''))); ?>    				
				</div>
				<div class="form-group">
					<?php echo form_label('Price', 'price', array('class' => 'control-label'))?>
					<?php echo form_input(array('id' => 'price', 'name' => 'price','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('price', (isset($data_event_ticket->price)) ? $data_event_ticket->price : ''))); ?>    				
				</div>
				<div class="form-group">
					<?php echo form_label('Total Spaces', 'total_spaces', array('class' => 'control-label'))?>
					<?php echo form_input(array('id' => 'total_spaces', 'name' => 'total_spaces','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('total_spaces', (isset($data_event_ticket->total_spaces)) ? $data_event_ticket->total_spaces : ''))); ?>  
				</div>	
				<div class="form-group">
					<?php echo form_label('Ticket Type', 'ticket_type', array('class' => 'control-label'))?>

					<?php echo form_label('Adult', 'adult') . form_radio(array("name"=>"ticket_type","id"=>"ticket_type","value"=>"1", 'checked'=>set_radio('ticket_type', (isset($data_event_ticket->ticket_type)) ? $data_event_ticket->ticket_type : '', FALSE))); ?>&nbsp;&nbsp;
					<?php echo form_label('Child', 'child') . form_radio(array("name"=>"ticket_type","id"=>"ticket_type","value"=>"2", 'checked'=>set_radio('ticket_type',  (isset($data_event_ticket->ticket_type)) ? $data_event_ticket->ticket_type : '', FALSE))); ?>&nbsp;&nbsp;
					<?php echo form_label('Baby', 'baby') . form_radio(array("name"=>"ticket_type","id"=>"ticket_type","value"=>"3", 'checked'=>set_radio('ticket_type',  (isset($data_event_ticket->ticket_type)) ? $data_event_ticket->ticket_type : '', FALSE))); ?>&nbsp;&nbsp;
											
					
					<?php echo form_input(array('id' => 'unique_id', 'name' => 'unique_id', 'class'=>'form-control', 'type' => 'hidden', 'value' => set_value('unique_id', (isset($unique_id)) ? $unique_id : ''))); ?>  	
					<?php echo form_input(array('id' => 'ticket_id', 'name' => 'ticket_id', 'class'=>'form-control', 'type' => 'hidden', 'value' => set_value('ticket_id', (isset($ticket_id)) ? $ticket_id : ''))); ?>	
				</div>
				<div class="form-group"><a href="javascript:void(0);" id="add_more">Add new ticket</a></div>
			</div>
			</div>
			<?php //<!-- End for Ticket Data --->?>
			</div>
			</div>
			<?php // start Offer data?>
			<div class="row"><div class="col-md-10"><h3>Add Offers</h3></div></div>
			<div class="row">
			<div class="col-md-10">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Offer Master</th>
						<th>Option</th>
						<th>Associate Offer with</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select name="offer_master" multiple id="offer_master" style="width:200px;">
								<?php
								if(isset($offers) && is_array($offers) && count($offers)>0)
								{
									foreach($offers as $offer)
									{
										echo '<option value="'.$offer->offer_id.'">'.$offer->title.'</option>'.PHP_EOL;						
									}
								}
								else
								{
									echo '<option value="0">No Offer Available</option>';
								}
								?>
								
								
							</select>
						</td>
						<td>
							<button type="button" class="btn btn-info" id="move_offer">Move Offer</button>
							<br><br><br>
							<button type="button" class="btn btn-danger" id="delete_offer">Delete Offer from Right</button>
						</td>
						<td><select name="offer_selected[]" multiple id="offer_selected" style="width:200px;">
							<?php
								if(isset($offers_selected) && is_array($offers_selected) && count($offers_selected)>0)
								{
									foreach($offers_selected as $offer)
									{
										echo '<option selected value="'.$offer->offer_id.'">'.$offer->title.'</option>'.PHP_EOL;						
									}
								}
								else
								{
									//echo '<option value="0">No Offer Available</option>';
								}
								?>
							</select>
							</td>
					</tr>
				</tbody>
			</table>
			</div>
			</div>
			<div class="row">
			<div class="form-group">	
				<?php echo form_input(array('id' => 'event_id', 'name' => 'event_id', 'class'=>'form-control', 'type' => 'hidden', 'value' => set_value('event_id', (isset($event_id)) ? $event_id : ''))); ?>
				<?php echo form_submit('eventForm', 'Submit', 'class="btn btn-primary"', 'tabindex =14', 'id="submit_form_event"')?>
			</div>	
			
			</fieldset>	
			</div>
			
		<?php echo form_close(); ?>
	</div>	
</div>
</div>

	
<script type="text/javascript" charset="utf-8" >
	$(document).ready(function($) {
		$('#move_offer').click(function(e){
			e.preventDefault();
			//$('#offer_master option:selected').remove().appendTo('#offer_selected').removeAttr('selected');
			$('#offer_master option:selected').remove().appendTo('#offer_selected');
		});

		$('#delete_offer').click(function(e){
			e.preventDefault();
			$('#offer_selected option:selected').remove().appendTo('#offer_master').removeAttr('selected');
		});

		
	});
</script>