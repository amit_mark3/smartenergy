<div class="row">
<div class="col-md-12">
	<div class="table-responsive">
		<table class="table">
		  <thead>
			<tr>
			  <th>Event Category</th>
			  <th>Status</th>
			  <th>Action</th>
			</tr>
		  </thead>
		  <tbody>
		   <?php 
			if($data_eventcategory['total_rows']>0){
				$row = $data_eventcategory['row'];	
				foreach($row as $eventcategory){
					$event_category = stripslashes($eventcategory->name);
					$event_category_id = stripslashes($eventcategory->category_id);
					$event_status = $eventcategory->status==1?'Active':'Inactive';
			?>	
			<tr id="event_category_<?php echo $event_category_id; ?>">
			    <td><?php echo $event_category; ?></td>
				<td><a href="<?php echo site_url(ADMIN_PATH. '/eventcategory/statusUpdate/'.$eventcategory->category_id); ?>"><?php echo $event_status; ?></a></td>
				<td><a href="<?php echo site_url(ADMIN_PATH. '/eventcategory/add/'.$event_category_id); ?>">Edit</a></td>
			</tr>	
			<?php 
					}
				}else{
					echo '<tr><td colspan="3">'.NO_RESULT_FOUND.'</td></tr>';	
				}  
			?>
			</tbody>
		</table>		
	</div>					  
</div>
</div>
	
