<div class="row">
<div class="col-md-12">
	<div class="content-box-large box-with-header">
		<?php echo form_open(NULL, 'id="form_event_category"', 'class="form-inline"'); ?>
			<fieldset>
			<div class="form-group">
				<?php echo form_label('Event Category ', 'name', array('class' => 'control-label'))?>
				<?php echo form_input(array('id' => 'name', 'name' => 'name','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('name', (isset($event_category ->name)) ? $event_category ->name : ''))); ?>    				
			</div>
			<div class="form-group">
				<?php echo form_label('Event Color ', 'name', array('class' => 'control-label'))?>
				<?php echo form_input(array('id' => 'event_color', 'name' => 'color','class'=>'form-control', 'type' => 'text',  'maxlength'=>'50', 'value' => set_value('color', (isset($event_category ->color)) ? $event_category ->color : ''))); ?>    				
			</div>
			<div class="form-group">
				<?php echo form_label('Event Category Short Content ', 'category_short_content', array('class' => 'control-label'))?>
				<?php 
				 $data = array(
			      'name'        => 'category_short_content',
			      'id'          => 'category_short_content',
			      'value'       => isset($event_category ->short_content) ? $event_category ->short_content : '',
			      'rows'        => '20',
			      'cols'        => '30',
			      'class'       => 'form-control editor_tinymce',
			    );

			  	echo form_textarea($data);
				?>
			</div>
			<div class="form-group">
				<?php echo form_label('Event Category Content ', 'category_content', array('class' => 'control-label'))?>
				<?php 
				 $data = array(
			      'name'        => 'category_content',
			      'id'          => 'category_content',
			      'value'       => isset($event_category ->content) ? $event_category ->content : '',
			      'rows'        => '20',
			      'cols'        => '30',
			      'class'       => 'form-control editor_tinymce',
			    );

			  	echo form_textarea($data);
				?>
			</div>
			<div class="form-group">
				<?php echo form_label('Event Terms & Conditions', 'category_terms', array('class' => 'control-label'))?>
				<?php 
				 $data = array(
			      'name'        => 'category_terms',
			      'id'          => 'category_terms',
			      'value'       => isset($event_category ->terms) ? $event_category ->terms : '',
			      'rows'        => '20',
			      'cols'        => '30',
			      'class'       => 'form-control editor_tinymce',
			    );

			  	echo form_textarea($data);
				?>
							
			</div>
			<div class="form-group">	
				<?php echo form_input(array('id' => 'category_id', 'name' => 'category_id', 'class'=>'form-control', 'type' => 'hidden', 'value' => set_value('category_id', (isset($category_id)) ? $category_id : ''))); ?>
				<?php echo form_submit('eventcategoryForm', 'Submit', 'class="btn btn-primary"', 'tabindex =14', 'id="submit_form_eventcategory"')?>
			</div>	
			
			</fieldset>
		<?php echo form_close(); ?>
	</div>
	
</div>
</div>
<script type="text/javascript" src="/assets/vendors/tinymce/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
	$('#event_color').colorpicker();
</script>
<script type="text/javascript">
  tinymce.init({
    selector: ".editor_tinymce",
    /*menubar:true,*/
    statusbar: false,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste textcolor"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic undrline| forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
	
