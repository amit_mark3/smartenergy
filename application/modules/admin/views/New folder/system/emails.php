<div class="row">
<div class="container-fluid">
  <div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="<?php echo $class_general;?>">
        <a href="#general_settings" aria-controls="general_settings" role="tab" data-toggle="tab">General Settings</a>
      </li>
      <li role="presentation" class="<?php echo $class_email;?>">
        <a href="#booking_emails" aria-controls="booking_emails" role="tab" data-toggle="tab">Booking Emails</a>
      </li>
      
    </ul>
  
    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane <?php echo $class_general;?>" id="general_settings">
<div class="well">
  <h3>Email General Settings</h3>
</div>
<div class="container-fluid">
   <form class= role="form" accept-charset="utf-8" method="post" action="<?php echo site_url("/admin/system/emails/edit");?>">
    <div class="form-group">
    <table class="table table-hover">
  <thead>
    <tr>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><label for="sender_email">Sender Email:</label></td>
      <td><input type="email" class="form-control" id="sender_email" name="sender_email" value="<?php echo $this->settings->getSystemValue('sender_email')?>"></td>
    </tr>
    <tr>
      <td><label for="sender_fromname">Sender From Name:</label></td>
      <td><input type="text" class="form-control" id="sender_fromname" name="sender_fromname" value="<?php echo $this->settings->getSystemValue('sender_fromname')?>"></td>
    </tr>
    
  </tbody>
</table>
      </div>
      <input type="hidden" name="action" value="save_settings_name">
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>
      </div>
      <div role="tabpanel" class="tab-pane <?php echo $class_email;?>" id="booking_emails">
      <div class="well">
        <h3>Booking Emails</h3>
      </div>
      <div class="container-fluid">
         <form class= role="form" accept-charset="utf-8" method="post" action="<?php echo site_url("/admin/system/emails/edit");?>">
          <div class="form-group">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><label for="booking_confirmed_subject">Booking Confirmed Subject:</label></td>
                  <td><input type="text" class="form-control" id="booking_confirmed_subject" name="booking_confirmed_subject" value="<?php echo $this->settings->getSystemValue('booking_confirmed_subject')?>"></td>
                </tr>
                <tr>
                  <td><label for="booking_confirmed_body">Booking Confirmed Body:</label></td>
                  <td>
                  <textarea class="editor_tinymce" name="booking_confirmed_body" id="booking_confirmed_body" style="width:98%;height:200px;"><?php echo $this->settings->getSystemValue('booking_confirmed_body')?></textarea>
                  </td>
                </tr>
                <tr>
                  <td><label for="booking_cancelled_subject">Booking Confirmed Subject:</label></td>
                  <td><input type="text" class="form-control" id="booking_cancelled_subject" name="booking_cancelled_subject" value="<?php echo $this->settings->getSystemValue('booking_cancelled_subject')?>"></td>
                </tr>
                <tr>
                  <td><label for="booking_cancelled_body">Booking Confirmed Body:</label></td>
                  <td>
                  <textarea class="editor_tinymce" name="booking_cancelled_body" id="booking_cancelled_body" style="width:98%;height:200px;"><?php echo $this->settings->getSystemValue('booking_cancelled_body')?></textarea>
                  </td>
                </tr>
                <tr>
                  <td><label for="booking_pending_subject">Booking Pending Subject:</label></td>
                  <td><input type="text" class="form-control" id="booking_pending_subject" name="booking_pending_subject" value="<?php echo $this->settings->getSystemValue('booking_pending_subject')?>"></td>
                </tr>
                <tr>
                  <td><label for="booking_pending_body">Booking Pending Body:</label></td>
                  <td>
                  <textarea class="editor_tinymce" name="booking_pending_body" id="booking_pending_body" style="width:98%;height:200px;"><?php echo $this->settings->getSystemValue('booking_pending_body')?></textarea>
                  </td>
                </tr>
              </tbody>
            </table>
            
            
          </div>
          <input type="hidden" name="action" value="save_settings_name">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="/assets/vendors/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({
    selector: ".editor_tinymce",
    /*menubar:true,*/
    statusbar: false,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste textcolor"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic undrline| forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>