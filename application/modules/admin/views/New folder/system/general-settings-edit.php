<div class="row">
<form action="<?php echo site_url("admin/system/general/edit");?>" method="POST" role="form" accept-charset="utf-8">
	<legend>Edit Setting</legend>

	<div class="form-group">
		<table class="table table-hover">
		<thead>
			<tr>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><label for="settings_value"><?php echo $settings_name;?></label>
				<input type="text" class="form-control" id="settings_value" name="settings_value" placeholder="Enter value..." value="<?php echo $settings_value;?>">
				<?php echo $settings_description;?>
				</td>
			</tr>
		</tbody>
	</table>

	</div>
	<input type="hidden" name="settings_id" value="<?php echo $settings_id;?>">
	<input type="hidden" name="action" value="save_settings">
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
	
</div>