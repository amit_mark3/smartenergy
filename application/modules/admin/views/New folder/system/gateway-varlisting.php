
<div class="row">
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Value</th>
        <th>Description</th>
      </tr>
    </thead>

    <tbody>
     <?php 
    foreach($query->result() as $row)
    {
      $id = $row->id;
      $name= $row->name;
      $value = $row->value;
      $description = $row->description;
      ?>
      <tr>
        <td><a href="<?php echo site_url('admin/system/gateways/edit/'.$id)?>"><?php echo $name;?></a></td>
        <td><?php echo $value;?></td>
        <td><?php echo $description;?></td>
        
      </tr>
      <?php
    }
    ?>
    </tbody>
  </table>
</div>