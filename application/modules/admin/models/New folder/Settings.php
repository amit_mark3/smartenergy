<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends SystemBase {

	public $variable;

	public function __construct()
	{
		parent::__construct();

	}
	public function getSystemValue($varname)
	{
		return parent::getSystemValue($varname);
	}
	public function getSystemValueRecordByID($id)
	{
		return parent::getSystemValueRecordByID($id);
	}
	public function getGeneralSettings()
	{
		$query = $this->db->query("select * from tbl_system_settings where type='G' order by id");
		return $this->load->view('system/varlisting', array('query'=>$query), TRUE);
	}
	public function getGatewaySettings()
	{
		$query = $this->db->query("select * from tbl_system_settings where type='P' order by id");
		return $this->load->view('system/gateway-varlisting', array('query'=>$query), TRUE);
	}
	public function saveSettings($id, $value)
	{
		if($id>0)
		{
			$query = $this->db->query("update tbl_system_settings set value = '$value' where id = $id");
			if($this->db->affected_rows())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}


	public function savegatewaysSettings($id, $value,$type)
	{
		if($id>0)
		{
			$query = $this->db->query("update tbl_system_settings set value = '$value' where id = $id and type='$type'");
			if($this->db->affected_rows())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}


	public function saveSettingsByName($name, $value)
	{
		if(strlen($name)>0)
		{
			$query = $this->db->query("update tbl_system_settings set value = '$value' where name = '$name'");
			if($this->db->affected_rows())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

}

/* End of file system.php */
/* Location: ./application/models/system.php */