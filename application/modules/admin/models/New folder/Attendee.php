<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendee extends AttendeeBase {

	public $variable;
	public $tbl_attendees = "attendees";

	public function __construct()
	{
		parent::__construct();

	}

	public function get_attendees_byid($id)
 	{
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from($this->tbl_attendees);
		$this->db->where($condition);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
		   return $query->result();
		}
		else
		{
		  return false;
	    }
   }

   public function get_attendees_detail($data='')
   {
		$condition = "id =" . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from($this->tbl_attendees);
		$this->db->where($condition);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
		   return $query->result();
		}
		else
		{
		  return false;
	   }
   }

public function getAttendeesByEventID($id, $fields='')
{
	$option = array('where'=>'event_id', 'data'=>$id);
	if(strlen($fields)>0)
	{
		$option['fields'] = $fields;
	}
	return parent::getAttendees($option);
}

public function getAttendeesByTicketID($id, $fields='')
{
	$option = array('where'=>'ticket_id', 'data'=>$id);
	if(strlen($fields)>0)
	{
		$option['fields'] = $fields;
	}
	return parent::getAttendees($option);
}

public function getAttendeesByBookingID($id, $fields='')
{
	$option = array('where'=>'booking_id', 'data'=>$id);
	if(strlen($fields)>0)
	{
		$option['fields'] = $fields;
	}
	return parent::getAttendees($option);
}

public function getAttendeesByAttendeeID($id, $fields='')
{
	$option = array('where'=>'id', 'data'=>$id);
	if(strlen($fields)>0)
	{
		$option['fields'] = $fields;
	}
	return parent::getAttendees($option);
}

}

/* End of file Attendee.php */
/* Location: ./application/models/Attendee.php */