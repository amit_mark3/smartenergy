<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends EventBase {

	public $variable;

	public $event_category = "event_category";
	public $events = "events";
	public $tickets = "tickets";
	public function __construct()
	{
		parent::__construct();

	}

	/***
	*
	* Display One RECORD in Event Category 22-06-2016
	*
	***/
	public function fetchOneEventCategory($where='') {
		$this->db->select('EC.* ');
	  $this->db->from($this->event_category." EC");
	  if($where!= '') {
			$this->db->where($where);
		}
		$sql = $this->db->get();
		//ECHO $this->db->last_query();
		$result = array();
		if($sql->num_rows()>0)
		{
			$result = $sql->result();
		}
		return $result[0];
   }
   /***
	*
	* Update Event Category 22-06-2016
	*
	***/
	Public function updateEventCategory($update_data,$id='')
    {
		$updt_data =0;
		if(!empty($id))
		{
			  $this->db->where('category_id',$id);
			  $updt_data = $this->db->update($this->event_category, $update_data);
			  // echo $this->db->last_query();	die;
		}
		return $updt_data;
	}
	/***
	*
	* Save Event Category 22-06-2016
	*
	***/
	function saveEventCategory($post)
    {
		if(!empty($post))
		{
			  $this->db->insert($this->event_category, $post);
			  $last_id= $this->db->insert_id();
			  return $last_id;
		}
	}
	/***
	*
	* Display all Event Category
	*
	***/
	function fetchAllEventCategory($data='')
	{
	  $rdata = array();

	  $where = '';
	  if(isset($data['where']) && $data['where']!= '') {
		$where = $data['where'];
	  }

	  $this->db->select(' EC.* ');
	  $this->db->from($this->event_category." EC");
	  //$this->db->join($this->tbl_events_details." ED","ED.event_id=T.event_id","inner");
	  //$this->db->join($this->tbl_event_photos." EP","EP.id=T.event_photos_id","LEFT");

	  if(isset($where) && $where!= '') {
	    $this->db->where($where);
	  }
	  if(isset($data['id']) && $data['id'] > 0)  {
		$id = $data['id'];
	    $this->db->where("EC.category_id",$id);
	  }
	  $sort = isset($data['sort'])?$data['sort'] : 'EC.category_id';
	  $order = isset($data['order'])?$data['order'] : 'desc';
	  $this->db->order_by($sort, $order);
	  //$data['pgno'] = 2;
	  //$data['perPage'] =10;
	  /*$pgno =  isset($data['pgno']) && $data['pgno']>=0?$data['pgno']:0;
	  $limit = isset($data['perPage']) && $data['perPage']>0?$data['perPage']: PER_PAGE;
	  $perPage =  ($pgno*$limit);
	  $this->db->limit($limit,$perPage); */
	  $sql = $this->db->get();
		//ECHO $this->db->last_query();
		$row ='';
	  if($sql->num_rows()>0)
	  {
		$row[] = $sql->result();
		$row = $row[0];
	  }
	  $rdata['row'] =$row;
	  if(isset($data['total_rows'])){
		  $this->db->flush_cache();
		  if(isset($where) && $where!= '') {
			$this->db->where($where);
		  }
		  $rdata['total_rows'] = $this->db->count_all_results($this->event_category." EC");
	  }
	  return $rdata;
	}

	
   /***
	*
	* Update Event 22-06-2016
	*
	***/
	Public function updateEvents($update_data,$id='')
    {
		$updt_data =0;
		if(!empty($id))
		{
			  $this->db->where('event_id',$id);
			  $updt_data = $this->db->update($this->events, $update_data);
			  // echo $this->db->last_query();	die;
		}
		return $updt_data;
	}
	/***
	*
	* Update Event Parent Id 22-06-2016
	*
	***/
	Public function updateEventsParentId($update_data,$parent_event_id='')
    {
		$updt_data =0;
		if(!empty($parent_event_id))
		{
			  $this->db->where('parent_event_id',$parent_event_id);
			  $updt_data = $this->db->update($this->events, $update_data);
			  // echo $this->db->last_query();	die;
		}
		return $updt_data;
	}
	/***
	*
	* Save Event 22-06-2016
	*
	***/
	function saveEvents($post)
    {
		if(!empty($post))
		{
			  $this->db->insert($this->events, $post);
			  $last_id= $this->db->insert_id();
			  return $last_id;
		}
	/***
	*
	* Save Event in batch 23-06-2016
	*
	***/
	function saveEventsBatch($post)
    {
		if(!empty($post))
		{
			  $this->db->insert_batch($this->events, $post);
			  $last_id= $this->db->insert_id();
			  return $last_id;
		}
	}
	}
	
	/***
	*
	* Display all Event 22-06-2016
	*
	***/
	function fetchAllEvents($data='')
	{
	  $rdata = array();
	  $event_id ='';
	  $where = '';
	  if(isset($data['where']) && $data['where']!= '') {
		$where = $data['where'];
	  }

	  $this->db->select(' EC.name AS category_name, E.* ');
	  $this->db->from($this->events." E");
	  $this->db->join($this->event_category." EC","EC.category_id=E.category_id","LEFT");
	  //$this->db->join($this->tbl_event_photos." EP","EP.id=T.event_photos_id","LEFT");

	  if(isset($where) && $where!= '') {
	    $this->db->where($where);
	  }

	  if(isset($data['event_id']) && $data['event_id'] > 0)  {
		$event_id = $data['event_id'];
	    $this->db->where("E.event_id",$event_id);
	  }
	  $sort = isset($data['sort'])?$data['sort'] : 'E.event_id';
	  $order = isset($data['order'])?$data['order'] : 'desc';
	  $this->db->order_by($sort, $order);
	  //$data['pgno'] = 2;
	  //$data['perPage'] =10;
	  /*$pgno =  isset($data['pgno']) && $data['pgno']>=0?$data['pgno']:0;
	  $limit = isset($data['perPage']) && $data['perPage']>0?$data['perPage']: PER_PAGE;
	  $perPage =  ($pgno*$limit);
	  $this->db->limit($limit,$perPage); */
	  $sql = $this->db->get();
		//ECHO $this->db->last_query();
		$row ='';
	  if($sql->num_rows()>0)
	  {
		$row[] = $sql->result();
		$row = $row[0];
	  }
	  $rdata['row'] =$row;
	  if(isset($data['total_rows'])){
		  $this->db->flush_cache();
		  if(isset($where) && $where!= '') {
			$this->db->where($where);
		  }
		  $rdata['total_rows'] = $this->db->count_all_results($this->events." E");
	  }
	  return $rdata;
	}
	/***
	*
	* Save Event Ticket 22-06-2016
	*
	***/
	function saveEventsTicket($post)
    {
		if(!empty($post))
		{
			  $this->db->insert($this->tickets, $post);
			  $last_id= $this->db->insert_id();
			  return $last_id;
		}
	}
	/*
	* save Events associated offers in events_offers
	*/
	public function saveEventsOffers($post='')
	{
		# code...
		if(!empty($post))
		{
			  $this->db->insert('event_offers', $post);
			  $last_id= $this->db->insert_id();
			  return $last_id;
		}
	}
	
	 /***
	*
	* Update Event Ticket 22-06-2016
	*
	***/
	Public function updateEventsTickets($update_data,$id='')
    {
		$updt_data =0;
		if(!empty($id))
		{
			  $this->db->where('ticket_id',$id);
			  $updt_data = $this->db->update($this->tickets, $update_data);
			  // echo $this->db->last_query();	die;
		}
		return $updt_data;
	}
	 /***
	*
	* Update Event Ticket where event_id is available 22-06-2016
	*
	***/
	Public function updateEventsTicketsIsEventId($update_data,$id='')
    {
		$updt_data =0;
		if(!empty($id))
		{
			  $this->db->where('event_id',$id);
			  $updt_data = $this->db->update($this->tickets, $update_data);
			  // echo $this->db->last_query();	die;
		}
		return $updt_data;
	}
	 /***
	*
	* Delete Event Ticket 22-06-2016
	*
	***/
	Public function deleteEventsTicket($id='')
    {

		if(!empty($id))
		{
			$this->db->where('ticket_id',$id);
			$del_data =$this->db->delete($this->tickets);
			// echo $this->db->last_query();	die;
		}
		return $del_data;
	}
	 /***
	*
	* Delete Event Ticket where event id  22-06-2016
	*
	***/
	Public function deleteEventsTicketEventId($event_id='')
    {

		if(!empty($event_id))
		{
			$this->db->where('event_id',$event_id);
			$del_data =$this->db->delete($this->tickets);
			// echo $this->db->last_query();	die;
		}
		return $del_data;
	}
	 /***
	*
	* Delete Event where event id  22-06-2016
	*
	***/
	Public function deleteEvents($event_id='')
    {

		if(!empty($event_id))
		{
			$this->db->where('event_id',$event_id);
			$del_data =$this->db->delete($this->events);
			// echo $this->db->last_query();	die;
		}
		return $del_data;
	}





}

/* End of file Event.php */
/* Location: ./application/models/Event.php */