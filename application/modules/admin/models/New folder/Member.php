<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends MemberBase {

	public $variable;

	public function __construct()
	{
		parent::__construct();

	}
	public function getMemberByEmail($id, $fields='')
	{
		$option = array('where'=>'email', 'data'=>$id);
		if(strlen($fields)>0)
		{
			$option['fields'] = $fields;
		}
		return parent::getMemberDetail($option);
	}

	public function getMemberByID($id, $fields='')
	{
		$option = array('where'=>'id', 'data'=>$id);
		if(strlen($fields)>0)
		{
			$option['fields'] = $fields;
		}
		return parent::getMemberDetail($option);
	}

}

/* End of file Booking.php */
/* Location: ./application/models/Booking.php */