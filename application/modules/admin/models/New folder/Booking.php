<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends BookingBase {

	public $variable;

	public function __construct()
	{
		parent::__construct();

	}
	public function getBookingByBookingID($id, $fields='')
	{
		$option = array('where'=>'booking_id', 'data'=>$id);
		if(strlen($fields)>0)
		{
			$option['fields'] = $fields;
		}
		return parent::getBooking($option);
	}

	public function getAttendeesByMemberID($id, $fields='')
	{
		$option = array('where'=>'member_id', 'data'=>$id);
		if(strlen($fields)>0)
		{
			$option['fields'] = $fields;
		}
		return parent::getBooking($option);
	}

}

/* End of file Booking.php */
/* Location: ./application/models/Booking.php */