<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offer extends EventBase {

	public $variable;
	public $tbl_offer ='offer_master';

	public function __construct()
	{
		parent::__construct();

	}

//    public function
	public function getOffers($event_id=0)
	{
		if($event_id>0)
		{
			$sql = $this->db->query("select offer_id from event_offers where event_id = $event_id");
			$csv = '';
			$query = null;
			$arr = array();
			if($sql->num_rows()>0)
			{
				
				foreach($sql->result() as $offer)
				{
					$arr[]=$offer->offer_id;
				}
			}
			if(count($arr))
			{
				$csv=implode(",", $arr);
			}
			$condition = '';
			if(strlen($csv))
			{
				$condition = ' and offer_id NOT IN ( '.$csv.' ) ';
			}
			$query = $this->db->query("select * from offer_master where status = 1 $condition order by offer_id");	
		}
		else
		{
			$query = $this->db->query("select * from offer_master where status = 1 order by offer_id");	
		}
		if($query->num_rows()>0)
			return $query->result();
		else
			return array();
	}
	public function getOffersSelected($event_id=0)
	{
		# code...
		if($event_id>0)
		{
			$sql = $this->db->query("select b.* from event_offers a , offer_master b where a.offer_id = b.offer_id and a.event_id = $event_id and b.status=1");
			if($sql->num_rows()>0)
			{
				return $sql->result();
			}
		}
		else
			return array();
	}


}

/* End of file Event.php */
/* Location: ./application/models/Event.php */