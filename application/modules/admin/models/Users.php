<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends UsersBase {

	private $_num_rows 		= 0;
	private $_found_rows 	= 0;
	private $_table 		= 'users'; 
	
	public function __construct()
	{
		parent::__construct();

	}
	/*public function check_password($username, $password)
	{
		return parent::check_password($username, $password);
	}*/

	
	function check_password($username, $password)
	{
	   $this -> db -> select('id, first_name, last_name, email,user_type,mobile,created_by, status');
	   $this -> db -> from('users');
	   $this -> db -> where('email', $username);
	   $this -> db -> where('password', MD5($password));
	   $this -> db -> limit(1);
	   $query = $this -> db -> get();
	   //ECHO $this->db->last_query();die;
		//print '<pre>'; print_r($query); print '</pre>'; die;
	   if($query -> num_rows() == 1){
		 return $query->result();
	   }else{
		 return false;
	   }
	}
	public function recover_password($post_data)
	{  
		// check the database and see if the email is registered with us
		$this->db->select('id, email, first_name, last_name');
		$this->db->from($this->_table . ' u');
		$this->db->where('u.email', $post_data['email']);
		$query = $this->db->get(); 

		if($query->num_rows())
		{
			return $query->result_array();  
		} 
		return FALSE;
	} 
	
	// Create User Login Log users_ci_logins_log
	public function add_users_ci_logins_log($post_data)
	{
		//if($this->db->insert('users_ci_logins_log', $post_data))
		$this->db->insert('users_ci_logins_log', $post_data);
		//echo $last_query = $this->db->last_query();	
		//return  $this->db->insert_id();
		//return FALSE;		
	}
	// Create User Login Log users_ci_logins_log update for logout details
	public function update_users_ci_logins_log($post_data, $update_row_id)
	{
		$this->db->trans_start();
		$this->db->where('sessionid', $update_row_id);
		$this->db->update('users_ci_logins_log', $post_data);
		$this->db->trans_complete(); 
		//echo $last_query = $this->db->last_query();	
		$this->db->trans_status();
		//die;
		
		//return 
	} 		

}

/* End of file Users.php */
/* Location: ./application/models/Users.php */