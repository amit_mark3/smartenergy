<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class MY_Controller extends CI_Controller {

	public $data;  
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct(); 
		$last_update = time();
		$this->output->set_header("HTTP/1.0 200 OK");
		$this->output->set_header("HTTP/1.1 200 OK");
		$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', $last_update).' GMT');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->delete_cache();
		
		if($this->config->item('enable_profiler') === TRUE) //  && ENVIRONMENT != 'production'
        {
        	$this->output->enable_profiler(TRUE); 
        }
        
        $this->data['title'] 			= $this->config->item('default_title');
        $this->data['meta_author'] 		= $this->config->item('meta_author');
        $this->data['tagline'] 			= $this->config->item('tagline');
		$this->data['site_url'] 		= site_url(); 
		//$this->data['themes_url'] 		= themes_url(); 
		//$this->data['front_theme_url'] 	= get_front_theme_url();
		//$this->data['vendors_url'] 		= get_vendors_url();
		//$this->data['front_js_url'] 	= get_front_js_url();
		$this->data['assets_url'] 		= assets_url();
		$this->data['document_root'] 	= FCPATH;

 		
 		if($this->session->userdata('admin_logged_in')) 
		{
			// used for views 
			$this->data['admin_logged_in']	= TRUE;
			$this->data['user_id']		= $this->session->userdata('id');
			$this->data['user_type']	= $this->session->userdata('user_type');
			
			// Used for navbar.php
			$this->data['userdata']['display_name'] 	= $this->session->userdata('display_name');  
			//$this->data['userdata']['avatar'] 			= $this->session->userdata('avatar'); 
			//$this->data['userdata']['arr_roles_name'] 	= $this->session->userdata('arr_user_roles_name'); 
		}
		else
		{ 
			// used for views
			$this->data['admin_logged_in']	= FALSE; 
			$this->data['user_type']	= FALSE; 
		}

	} // end of constructor

	/** 
	 * is_logged_in
	 *
	 * Method to check if user is logged in 
	 * 
	 * @access protected 
	 * @param   
	 * @return boolean 
	 */

	protected function user_is_logged_in()
	{
		if($this->session->userdata('admin_logged_in') !== TRUE)
		{ 
			$this->session->set_flashdata('msg_error', $this->lang->line('common_not_logged_in'));
			redirect('login.html');
			return FALSE;
		}
		return TRUE;
	}
	
	/** 
	 * load_admin_view
	 *
	 * Method to load admin views 
	 
	 * @return  
	 */

	protected function load_admin_view($page = '', $breadcrumb = TRUE, $navbar = TRUE, $header = TRUE, $footer = TRUE)
	{
		//echo $this->config->item('admin_theme') . '/templates/header';die;
		if($header === TRUE)
		{
			$this->load->view($this->config->item('admin_theme') . '/templates/header', $this->data); 
		}

		if($navbar === TRUE)
		{
			$this->load->view($this->config->item('admin_theme') . '/templates/navbar', $this->data);
		} 
		 
		if($breadcrumb === TRUE)
		{
			$this->load->view($this->config->item('admin_theme') . '/templates/breadcrumb', $this->data);
		}

		if( !empty($page))
		{
			$this->load->view($page, $this->data);
		} 

		if($footer === TRUE)
		{
			$this->load->view($this->config->item('admin_theme') . '/templates/footer', $this->data);
		}
	}



}
// END Common Controller class

/* End of file MY_Controller.php */ 
/* Location: ./application/core/MY_Controller.php */
