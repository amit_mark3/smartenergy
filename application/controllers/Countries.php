<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countries extends MY_Controller {
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		// this controller need users to be logged in 
		// $this->user_is_logged_in(); 
	} 

	/** 
	 * get_countries_list  
	 * Default Method to get the counties list
	 * 
	 * @access public 
	 * @param  
	 * @return   
	 */
	public function get_countries_list($ajax = TRUE, $id = FALSE, $search_clause = array(), $search_string = '', $limit = 0, $offset = 0, $sort_by ='', $sort_order = '') 
	{
		$this->load->model('common_model');   

		if($ajax === TRUE && $id === FALSE)
		{
			$id = ($this->input->post('id')) ? $this->input->post('id') : FALSE;  
			$search_string = ($this->input->post('search_string')) ? $this->input->post('search_string') : '';   
		} 

 		$countries = $this->common_model->get_countries($limit, $offset, $sort_by, $sort_order, $id, $search_clause, $search_string); 

		if($ajax === TRUE || $this->input->is_ajax_request())
		{ 
			header('Content-Type: application/json');
			echo json_encode($countries);
        	exit;
        }
        else
        {
        	return $countries;
        }

	}   

	/** 
	 * get_states_list  
	 * Default Method to get the states list
	 * 
	 * @access public 
	 * @param  
	 * @return   
	 */
	public function get_states_list($ajax = TRUE, $id = FALSE, $search_clause = array(), $search_string = '', $limit = 0, $offset = 0, $sort_by ='', $sort_order = '') 
	{
		$this->load->model('common_model');   

		if($ajax === TRUE && $id === FALSE)
		{
			$id = ($this->input->post('id')) ? $this->input->post('id') : FALSE;  
			$search_string = ($this->input->post('search_string')) ? $this->input->post('search_string') : '';   
		} 

 		$states = $this->common_model->get_states($limit, $offset, $sort_by, $sort_order, $id, $search_clause, $search_string);  

		if($ajax === TRUE || $this->input->is_ajax_request())
		{ 
			header('Content-Type: application/json');
			echo json_encode($states);
        	exit;
        }
        else
        {
        	return $states;
        }

	}  

	/** 
	 * get_districts_list  
	 * Default Method to get the districts list
	 * 
	 * @access public 
	 * @param  
	 * @return   
	 */
	public function get_districts_list($ajax = TRUE, $id = FALSE, $search_clause = array(), $search_string = '', $limit = 0, $offset = 0, $sort_by ='', $sort_order = '') 
	{
		$this->load->model('common_model');   

		if($ajax === TRUE && $id === FALSE)
		{
			$id = ($this->input->post('id')) ? $this->input->post('id') : FALSE;  
			$search_string = ($this->input->post('search_string')) ? $this->input->post('search_string') : '';   
		} 

 		$districts = $this->common_model->get_districts($limit, $offset, $sort_by, $sort_order, $id, $search_clause, $search_string);  

		if($ajax === TRUE || $this->input->is_ajax_request())
		{ 
			header('Content-Type: application/json');
			echo json_encode($districts);
        	exit;
        }
        else
        {
        	return $districts;
        }

	}  
}
// END countries Controller class
/* End of file countries.php */
/* Location: ./application/controllers/countries.php */