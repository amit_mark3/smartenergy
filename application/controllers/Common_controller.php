<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_controller extends MY_Controller {
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		// this controller need users to be logged in 
		$this->user_is_logged_in();
	} 
}
// END Common Controller class
/* End of file common_controller.php */
/* Location: ./application/controllers/common_controller.php */