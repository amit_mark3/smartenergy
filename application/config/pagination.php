<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
| -------------------------------------------------------------------
| pagination
| -------------------------------------------------------------------
| This file contains an array of pagination configuration for use with the pagination Class.
|
*/ 

$config['full_tag_open'] = '<div class="dataTables_paginate paging_simple_numbers" id="pagination-container"><ul class="pagination">';

$config['full_tag_close'] = '</ul></div>';
                    
$config['first_link'] = '&larr; Newer';

$config['first_tag_open'] = '<li class="previous">';

$config['first_tag_close'] = '</li>';

$config['last_link'] = 'Older &rarr;';

$config['last_tag_open'] = '<li class="next">';

$config['last_tag_close'] = '</li>';

$config['prev_link'] = 'Previous';

$config['prev_tag_open'] = '<li class="paginate_button previous" aria-controls="sample-table-2" tabindex="0" id="sample-table-2_previous">';

$config['prev_tag_close'] = '</li>';

$config['next_link'] = 'Next';

$config['next_tag_open'] = '<li class="paginate_button next" aria-controls="sample-table-2" tabindex="0" id="sample-table-2_next">';

$config['next_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="paginate_button active" aria-controls="sample-table-2" tabindex="0"><a href="#">';

$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li class="paginate_button " aria-controls="sample-table-2" tabindex="0">';

$config['num_tag_close'] = '</li>';

$config['use_page_numbers'] = FALSE;

$config["num_links"] = 2;

$config['query_string_segment'] = 'offset';

/** 
 * Following config items need to define in cotrollers if want to use pagination in multiple pages
 * $config['page_query_string'] = TRUE;
 * $config['base_url'] = '';
 * $config['uri_segment'] = 4;
 * $config['total_rows'] = 20;
 * $config['per_page'] = 5;
 * $choice = $config["total_rows"] / $config["per_page"];
 * $config["num_links"] = round($choice);
 * $this->pagination->initialize($config);
 */

/* End of file pagination.php */
/* Location: ./application/config/pagination.php */