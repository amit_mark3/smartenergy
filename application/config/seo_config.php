<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['seo_title']  = 'Smart Energy';
$config['seo_desc']   = 'Smart Energy';
$config['seo_keyword']   = 'Smart, Energy, Meter, Light';
$config['seo_robot']  = true;

/* End of file seo_config.php */
/* Location: ./application/config/seo_config.php */