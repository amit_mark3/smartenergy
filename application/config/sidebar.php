<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$config['sidebar']										= array();

// ========================================= Dashboard ========================================= //

/* ============= Dashboard ============= */
$config['sidebar']['dash']['modules']					= array('users', 'admin');
$config['sidebar']['dash']['controllers']				= array('dashboard', 'error');
$config['sidebar']['dash']['functions']					= array('index');
 

// ========================================= Users ========================================= //

/* ============= Users ============= */
$config['sidebar']['users']['modules']					= array('users');
$config['sidebar']['users']['controllers']				= array('users', 'staff');
$config['sidebar']['users']['functions']				= array('customers', 'vendors', 'all_users');
$config['sidebar']['users']['staff_functions']			= array('staff', 'add');

  
// ========================================= Settings ========================================= //

/* ============= Settings ============= */
$config['sidebar']['settings']['modules']				= array('users', 'acl', 'cities');
$config['sidebar']['settings']['controllers']			= array('profile', 'password', 'assign_roles', 'roles', 'permissions', 'mapping', 'cities');
$config['sidebar']['settings']['functions']				= array('index', 'listing', 'add', 'edit', 'status');


/* ============= Access Control List ============= */
$config['sidebar']['acl']['modules']					= array('acl');
$config['sidebar']['acl']['controllers']				= array('roles', 'permissions', 'mapping');
$config['sidebar']['acl']['roles_controllers']			= array('roles', 'mapping');
$config['sidebar']['acl']['functions']					= array('index', 'listing', 'add', 'edit', 'status');
$config['sidebar']['acl']['roles_view_functions']		= array('index', 'listing', 'status');


/* ============= Localisation ============= */
$config['sidebar']['localisation']['modules']			= array('cities');
$config['sidebar']['localisation']['controllers']		= array('cities');
$config['sidebar']['localisation']['functions']			= array('index', 'listing', 'add', 'edit', 'status');


/* End of file sidebar.php */
/* Location: ./application/config/sidebar.php */