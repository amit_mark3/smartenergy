<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$config['default_title'] = 'Smart Energy';

$config['tagline']	= 'Smart Energy';

$config['meta_author'] = 'Smart Energy';

$config['front_theme']	= 'front_theme';

$config['admin_theme']	= 'admin_theme';

$config['records_per_page']	= 10;

$config['language']	= 'english'; // what is this for why this giving error if removed?

$config['language_id']	= '1';

$config['module_uri_segment']	= 1;

$config['controller_uri_segment']	= 2;

$config['function_uri_segment']	= 3; 

$config['app_from_email']	= 'career@mark3.in'; 

$config['app_from_name']	= 'Smart Energy'; 

$config['app_alerts_to_email']	= 'career@mark3.in'; 

$config['user_types'] = array('Individual' => 'Individual', 'Internal User' => 'Internal User');

$config['default_roles'] = array('0' => 'application_owner', '1' => 'admin', '2' => 'user');  // key is role id and value is Machine Names

$config['status_options'] = array('Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted');

/* End of file my_config.php */
/* Location: ./application/config/my_config.php */