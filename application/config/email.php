<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
| -------------------------------------------------------------------
| email
| -------------------------------------------------------------------
| This file contains an array of email configuration for use with the email Class.
|
*/
$config['useragent'] 		= 'Email Sender Name'; // The "user agent".
$config['protocol'] 		= 'mail'; // mail, sendmail, or smtp	The mail sending protocol.
$config['mailpath']			= '/usr/sbin/sendmail'; //The server path to Sendmail.
$config['smtp_host']		= ''; // SMTP Server Address.
$config['smtp_user']		= ''; // SMTP Username.
$config['smtp_pass']		= ''; // SMTP Password.
$config['smtp_port']		= 25; // SMTP Port.
$config['smtp_timeout']		= 5; // SMTP Timeout (in seconds).
$config['wordwrap']			= TRUE; // TRUE or FALSE (boolean)	Enable word-wrap.
$config['wrapchars']		= 76; // Character count to wrap at.
$config['mailtype']			= 'html'; // text or html	Type of mail. 
$config['charset']			= 'utf-8'; // Character set (utf-8, iso-8859-1, etc.).
$config['validate']			= FALSE; //	TRUE or FALSE (boolean)	Whether to validate the email address.
$config['priority']			= 1; // 1, 2, 3, 4, 5	Email Priority. 1 = highest. 5 = lowest. 3 = normal.
$config['crlf'] 			= '\n'; // "\r\n" or "\n" or "\r" 	Newline character. (Use "\r\n" to comply with RFC 822).
$config['newline']			= '\n'; // "\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
$config['bcc_batch_mode']	= FALSE; //	TRUE or FALSE (boolean)	Enable BCC Batch Mode.
$config['bcc_batch_size']	= 200; // Number of emails in each BCC batch.
/* End of file email.php */
/* Location: ./application/config/email.php */