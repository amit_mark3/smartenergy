<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * reCaptcha File Config
 *
 * File     : recaptcha.php
 * Created  : https://github.com/google/recaptcha
 * 
 * Author   : https://github.com/Cnordbo/RECaptcha-for-Codeigniter
 */

/*
|--------------------------------------------------------------------------
| Site key
|--------------------------------------------------------------------------
|
| Use this in the HTML code your site serves to users. 
|
| https://www.google.com/recaptcha/admin#site/320642516?setup
| 
| Local
| 6LfUnRwTAAAAAHh3j0lUxkjiCnt6FR_yJ83EEC19
| 6LfUnRwTAAAAAKYYF5XBoopnIfS9CyL4ZtAKACzL
|
*/ 
$config['site_key']   = '6LcjHwoUAAAAAB_C-_qPfn5xjNvIRMneX_fbYBOQ';

/*
|--------------------------------------------------------------------------
| Secret key
|--------------------------------------------------------------------------
|
| Use this for communication between your site and Google. Be sure to keep it a secret.
|
| https://www.google.com/recaptcha/admin#site/320642516?setup
| 
|
*/ 

$config['secret_key']  = '6LcjHwoUAAAAAL20VKQmSna7syEtO_J2uoqvvCFH';

/*
|--------------------------------------------------------------------------
| Set Recaptcha options
|--------------------------------------------------------------------------
|
| Set Recaptcha options
|
| Reference at https://developers.google.com/recaptcha/docs/customization
| 
|
*/  
$config['recaptcha_options']  = array(
    'theme'=>'red', // red/white/blackglass/clean
    'lang' => 'en' // en/nl/fl/de/pt/ru/es/tr
    //  'custom_translations' - Use this to specify custom translations of reCAPTCHA strings.
    //  'custom_theme_widget' - When using custom theming, this is a div element which contains the widget. See the custom theming section for how to use this.
    //  'tabindex' - Sets a tabindex for the reCAPTCHA text box. If other elements in the form use a tabindex, this should be set so that navigation is easier for the user
); 