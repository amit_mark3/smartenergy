<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * get_delete_confirm_html
 *
 * Returns the delete Popup HTML
 *
 * @param	
 * @return	string
 */
if ( ! function_exists('get_delete_confirm_html'))
{
	function get_delete_confirm_html($message = 'Do you want to delete this Item?')
	{
		return '
		<div id="dialog-delete-confirm" class="hide">
		    <div class="alert alert-info bigger-110">
		    	'. $message .'
		    </div>

		    <div class="space-6"></div>

		    <p class="bigger-110 bolder center grey">
		        <i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>
		        Are you sure?
		    </p>
		</div><!-- #dialog-confirm -->
    	';
	}
}

// ------------------------------------------------------------------------

/**
 * generate_random_code
 *
 * Returns the random code
 *
 * @param	string - name
 * @param	int - param id
 * @return	string
 */
if ( ! function_exists('generate_random_code'))
{
	function generate_random_code($name, $random_id_1 = '', $random_id_2 = '')
	{ 
		$pos = strpos($name, ' ', 5);
		$code = substr($name, 0, $pos);  
		$new_code = str_replace(' ', '_', $code); 
		$return_string = $new_code . '_' . date('Y_m_d') . '_' . $random_id_1;
		if( ! empty($random_id_2))
		{
			$return_string .= '_' .  $random_id_2;
		}
		return $return_string;
	}
}

/**
 * generate_random_otp
 *
 * Returns the random OTP
 *
 * @param	int - min 
 * @param	int - max
 * @return	int
 */
if ( ! function_exists('generate_random_otp'))
{
	function generate_random_otp($min = '100000', $max = '999999')
	{ 
		return rand($min, $max); 
	}
}
// ------------------------------------------------------------------------
	


if ( ! function_exists('send_email'))
{
	function send_email($param_email)
	{   
		//to,subject,message,cc,bcc,reply,attached
		/*	mail($to, $subject, $message, implode("\r\n", $headers));	*/
		$email_message = email_html_format(trim($param_email['message']));
		
		//echo $email_message; 
		$email_to = trim($param_email['to']) == ''?'career@mark3.in':$param_email['to'];
		$email_cc = isset($param_email['cc'])?$param_email['cc']:""; 
		$email_bcc = isset($param_email['bcc']) ?$param_email['bcc']:""; 
		$email_reply = isset($param_email['reply'])?$param_email['reply']:""; 
		$email_attached = isset($param_email['attached'])?$param_email['attached']:""; 
		$email_subject = trim($param_email['subject']) == ''?"Smart Energy":$param_email['subject']; 
		
		$CI =& get_instance();
		$CI->load->library('email');
		$config['mailtype'] = 'html';
		$config['priority'] = 1;
		$config['charset'] = 'utf-8';
		//$config['charset'] = 'Content-type: text/html; charset=iso-8859-1';
		$CI->email->initialize($config);
		$CI->email->set_crlf( "\r\n" );
		$email = $CI->email;
		
        $email->clear();
        $email->from($CI->config->item('app_from_email'), $CI->config->item('app_from_name'));
        $email->to($email_to);
		//$cid = $this->email->attachment_cid($filename);
		//CC
		if(trim($email_cc)!=''){
			$email->cc($email_cc);
		}
		//Bcc
		if(trim($email_bcc)!=''){
			$email->bcc($email_bcc);
		}
		if(trim($email_reply)!=''){
			$email->reply_to($email_reply);
		}
		if(trim($email_attached)!=''){
			$email->attach($email_attached);
		}
        $email->subject($email_subject);
        $email->message($email_message);
        $return['email_send_flag'] = $email->send();
        $return['email_send_debug'] = $email->print_debugger();
        //echo "<pre>"; print_r($return);  echo $email_message; die;
        return $return;
	}
}
//HTML Mail Format
if ( ! function_exists('email_html_format'))
{
	function email_html_format($message)
	{ 
		$email_html_data = '
		<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
	
<style type="text/css">
	#outlook a {
	  padding: 0;
	}
	body {
	  width: 100% !important;
	  -webkit-text-size-adjust: 100%;
	  -ms-text-size-adjust: 100%;
	  margin: 0;
	  padding: 0;
	}
	.ExternalClass {
	  width: 100%;
	}
	.ExternalClass,
	.ExternalClass span,
	.ExternalClass font,
	.ExternalClass td,
	.ExternalClass div {
	  line-height: 100%;
	}
	.ExternalClass p {
	  line-height: inherit;
	}
	#body-layout {
	  margin: 0;
	  padding: 0;
	  width: 100% !important;
	  line-height: 100% !important;
	}
	img {
	  display: block;
	  outline: none;
	  text-decoration: none;
	  -ms-interpolation-mode: bicubic;
	}
	a img {
	  border: none;
	}
	table td {
	  border-collapse: collapse;
	}
	table {
	  border-collapse: collapse;
	  mso-table-lspace: 0pt;
	  mso-table-rspace: 0pt;
	}
	
	a {
	  color: orange;
	  outline: none;
	}
	table th p{
		  text-align: justify !important;
	  }
	  
	.leftColumnContent.detail strong{margin-top:6px; display:block; padding:0 12px;}
	.section .detail strong{padding:0;}
	.section td{padding:3px 0 8px;}
	.section th{padding:5px 5px;}
	.section th strong{font-weight:600;}
	.table_data th{background:#eeeeee; border:1px solid #ddd; font-weight:600;}
	.table_data td:first-child{border-left:1px solid #dddddd;}
	.table_data td{padding:8px 5px; border-bottom:1px solid #dddddd; border-right:1px solid #dddddd;}
	.table_data :tr:last-child td{border-bottom:none;}
</style>

<style type="text/css">
	@media only screen and (max-width: 640px) {
	  table[class="inner"] {
		width: 100% !important;
		max-width: 100% !important;
	  }
	  td[background] {
		background-size: cover !important;
	  }
	}
	@media only screen and (max-width: 599px) {
	  table td[class="block"] {
		width: 100% !important;
		max-width: 375px !important;
		display:block !important;
		font-size:14px;
		border-right:none !important;
	  }
	  table td img{
		  margin:0 auto;
	  }
	  table td h1{
		  font-size:20px !important;
	  }
	  
	  table td[class="block-200"] {
		width: 100% !important;
		max-width: 200px !important;
	  }
	  table[class="center"],
	  td[class="center"] {
		text-align: center !important;
		float: none !important;
	  }
	  table[class="remove"],
	  tr[class="remove"],
	  span[class="remove"] {
		display: none;
	  }
	  a[class="unsubscribe"] {
		display: block !important;
		background: #3a424c !important;
		text-transform: uppercase !important;
		color: #ffffff !important;
		text-decoration: none !important;
		padding: 5px 0 !important;
		border-radius: 4px !important;
	  }
	  a[class="webversion"] {
		display: block !important;
		margin-top: 12px !important;
		background: #3a424c !important;
		text-transform: uppercase !important;
		color: #ffffff !important;
		text-decoration: none !important;
		padding: 5px 0 !important;
		border-radius: 4px !important;
	  }
	}
</style>
	
			</head>
				<body>
					<table border="0" cellpadding="0" cellspacing="0" width="80%" align="center" >
					<tr>
						<td valign="middle" width="100%">
							<table border="0" cellpadding="8" cellspacing="0" width="100%" style="background:#376900;">
								<tr>
									<td style="border-right:1px solid #00481f; width:180px;" class="block">
										<img src="'.site_url("email-logo.png").'" />
									</td>
									<td valign="middle" class="block">
										
										<h1 style="color:#fff; font-size:22px; text-transform:uppercase; margin:0px 0 0 0; padding:0; font-weight:500; text-align:center; font-family: arial;">INDIAN COOPERATIVE DIGITAL PLATFORM</h1>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td valign="middle" width="100%">
							<table border="0" cellpadding="8" cellspacing="0" width="100%" style="border-left:1px solid #eeeeee; border-right:1px solid #eeeeee;">
								<tr>
									<th style="text-align:left; font-family:arial; font-weight:500; font-size:100%;">'.$message.'</th>
								</tr>									 
							</table>
						</td>
					</tr>
					<tr>
						<td valign="middle" width="100%" class="templateColumnContainer">
							<table border="0" cellpadding="8" cellspacing="0" width="100%" style="background:#000000; text-align:center;">
								<tr>
									<td style="font-size:14px; color:#ffffff; font-family: arial; font-weight:600;">
										Copyright © '.date('Y').' Smart Energy. All rights Reserved
									<td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

				</body>
			</html>';
			
		
		return $email_html_data;
	}
}
/* End of file functions_helper.php */
/* Location: ./application/helpers/functions_helper.php */