<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * get_paging_info
 *
 * Returns the pagination info
 * @param int
 * @param int
 * @param int
 * @param int
 * @return  string
 */
if ( ! function_exists('get_paging_info'))
{
    function get_paging_info($offset, $limit, $fetched_rows, $found_rows)
    {
        $X = ($found_rows) ? $offset + 1 : $offset;
        $Y = $offset + $limit;
        if($fetched_rows < $limit)
        {
            $Y -= ($limit - $fetched_rows); 
        }
        $CI =& get_instance();
        $CI->lang->load('common', $CI->config->item('language'));
        $info = '<div id="sample-table-2_info" class="dataTables_info" role="status" aria-live="polite">';
        $info .= sprintf($CI->lang->line('common_paging_info'), $X , $Y, $found_rows, ceil($found_rows / $limit));
        $info .= '</div>';
        return $info; 
    }
}

//

// ------------------------------------------------------------------------

/**
 * get_paging_limit_options
 *
 * Returns the pagination limit drop down 
 * @param string
 * @param int
 * @param int
 * @return  string
 */
if ( ! function_exists('get_paging_limit_options'))
{
    function get_paging_limit_options($url, $limit = '', $records = 0)
    {
        $CI =& get_instance();
        $CI->load->helper('form');
        $dropdown = '';
        $attributes = array('class' => 'page-limit', 'id' => 'paging_limit');
        $dropdown .= form_open($url, $attributes); 
        $dropdown .= '<div class="dataTables_length pull-right" id="sample-table-2_length">';
        $dropdown .= '<label for="paging_limit">Display';
        $options = array();
        $records = ($records > 0) ? $records : 10; 
        $j = ($records && $records <= 100) ? $records : 100; 
        $i = 0;
        while($i < $j)
        {
            $i += 10;
            $options[$i] = $i;
        }
        $params = 'class="form-control input-sm" id="" onchange="this.form.submit();" aria-controls="sample-table-2"';
        $dropdown .= form_dropdown('paging_limit', $options, $limit, $params);
        $dropdown .= ' of total <span class="white bolder">'.$records.'</span> records</label></div>';
        $dropdown .= form_close();
        return $dropdown;
    }
}

// ------------------------------------------------------------------------

/**
 * get_data_list_sorting_heading
 *
 * Returns the sorting heading for a data grid
 * @param array
 * @param string
 * @param string
 * @param string
 * @param int
 * @param string
 * @return  array
 */
if ( ! function_exists('get_data_list_sorting_heading'))
{
    function get_data_list_sorting_heading($fields, $sort_by, $sort_order, $method_url, $limit, $search_query_string = '')
    {
        // set the list headings
        $table_headings = array();
        foreach($fields as $key => $field) 
        {
            $attributes = isset($field['attributes']) ? $field['attributes'] : '';
            $th_html = '<th '. $attributes .'>';
            if($field['sort'] === FALSE)
            {
                $th_html .= $field['display']; 
            }
            else
            {           
                $order_by = ($sort_order === 'asc' && $sort_by === $field['field']) ? 'desc' : 'asc';
                if($sort_by === $field['field']) 
                {     
                    $icon = '&nbsp; <i class="ace-icon fa fa-sort-' . $sort_order . '"></i>'; 
                }
                else
                {
                    $icon = '&nbsp; <i class="ace-icon fa fa-sort"></i>'; 
                }

                $th_html .= anchor($method_url . $field['field'] . '/' . $order_by . '/' . $limit . '/?'.$search_query_string, $field['display'].$icon);
            } 
            $th_html .= '</th>';
            $table_headings[$field['field']] = $th_html;

        }
        return $table_headings;
    }
}

// ------------------------------------------------------------------------

/**
 * get_data_list_sort_by_dropdown
 *
 * Returns the sorting options dropdown
 * @param array
 * @param string
 * @param string
 * @param string
 * @param int
 * @param string
 * @return  array
 */
if ( ! function_exists('get_data_list_sort_by_dropdown'))
{
    function get_data_list_sort_by_dropdown($fields, $sort_by, $sort_order, $method_url, $limit, $search_query_string = '')
    {
        $CI =& get_instance();
        $CI->load->helper('form');
        $dropdown_options = array();
        $x = 0;
        $first_option = 'Select';  
        foreach($fields as $key => $field) 
        {
            $order_by = ($sort_order === 'asc' && $sort_by === $field['field']) ? 'desc' : 'asc';
            if($sort_by === $field['field']) 
            {     
                $icon = '&nbsp; (' . $order_by . ')'; 
            }
            else
            {
                $icon = '&nbsp; (asc)'; 
            }

            $dropdown_options[$x]['link'] = site_url($method_url . $field['field'] . '/' . $order_by . '/' . $limit . '/?'.$search_query_string);

            
            if($sort_by == $field['field']) 
            {     
                $first_option = '=='.$field['display'].'&nbsp; (' . $sort_order . ')==';
            } 

            $dropdown_options[$x]['display'] = $field['display'].$icon;
            $x++;
        }

        $search_form_attributes['attributes'] = array('class' => 'form-horizontal', 'id' => 'frm-sort-by', 'method' => 'get'); 

        $sort_by_dropdown = form_open($method_url, $search_form_attributes['attributes']); 

        $sort_by_dropdown .= '<div class="center"><label>Sort By:';


        $first_option1 = ($sort_by) ?  $sort_by.'&nbsp; (' . $sort_order . ')' : 'Select'; 
        $params = 'class="input-large" id="sort-by" onChange="window.location.href=this.value"';
        $sort_by_dropdown .= get_dropdown_html('sort_by', $dropdown_options, '', $params, '', $first_option, 'link', 'display');

        $sort_by_dropdown .=  '</label>';

        $sort_by_dropdown .= '</div>';

        $sort_by_dropdown .= form_close();
        return $sort_by_dropdown;
    }
}
// ------------------------------------------------------------------------


/**
 * get_record_list_search
 *
 * Returns the record list search
 *
 * 
 * @param  string   url of the page where search form will be posted
 * @param  string   search form id
 * @param  array    search fields
 * @param  int      record limit
 * @param  array    additional hidden fields
 * @return  string
 */
if ( ! function_exists('get_record_list_search'))
{
    function get_record_list_search($url, $form_id, $search_fields, $limit, $hidden_fields = array())
    {
        $CI =& get_instance();
        $CI->load->helper('form');
        
        $search_handler = array('search_clause' => array(), 'data' => array(), 'search_query_string' => '', 'search_clause_string' => '');

        $search_handler['data']['q'] = '';

        $search_form_attributes['attributes'] = array('class' => 'form-horizontal', 'id' => $form_id, 'method' => 'get');

        $search_form_attributes['hidden_fields'] = array('frm_search' => 1, 'paging_limit' => $limit);

        $search_form_attributes['hidden_fields'] = array_merge($search_form_attributes['hidden_fields'], $hidden_fields); 

        $search_handler['data']['search_form'] = form_open($url, $search_form_attributes['attributes'], $search_form_attributes['hidden_fields']); 

        $search_handler['data']['search_form'] .= '<div id="sample-table-2_filter" class="dataTables_filter list-search"><label>Search:';

        if($CI->input->get('frm_search') === '1')
        {
            $search_string = trim($CI->input->get('q'));
            if( ! empty($search_string)) 
            {
                $search_handler['search_query_string'] = "frm_search=1";
                $search_handler['search_query_string'] .= "&q=$search_string";
                $search_handler['data']['q'] = $search_string;
                $search_handler['search_clause_string'] = "(";
                $i = 0;
                foreach ($search_fields as $key => $field) 
                {
                    if($i !== 0)
                        $search_handler['search_clause_string'] .= " OR ";
                    // create array of fields and values for passing array of search params in functions like or_like $this->db->or_like($where_clause);
                    $search_handler['search_clause'][$field] = $search_string;
                    // create where clause string for $this->db->where($where_string, NULL, FALSE); 
                    $search_handler['search_clause_string'] .= $field . " LIKE '%" .$search_string . "%'";
                    $i++;
                }
                $search_handler['search_clause_string'] .= ")";
            }
        }

        $search_field_param = array(
            'name'        => 'q',
            'id'          => 'q',
            'value'       => $search_handler['data']['q'],
            'maxlength'   => '',
            'size'        => '',
            'style'       => '',
            'aria-controls' => 'sample-table-2',
            'placeholder' => 'Search',
            'class' => 'form-control input-sm',
        );
        $search_handler['data']['search_form'] .= form_input($search_field_param);
        $search_handler['data']['search_form'] .=  '</label>';


        $submit_button_param = array(
            'name' => 'submit',
            'id' => 'submit',
            'value' => 'true',
            'type' => 'submit',
            'content' => 'Go <i class="icon-arrow-right icon-on-right"></i>',
            'class' => 'btn btn-minier btn-success'
        );

        $search_handler['data']['search_form'] .= form_button($submit_button_param);

        $search_handler['data']['search_form'] .= '</div>';

        $search_handler['data']['search_form'] .= form_close();

        return $search_handler;
    }
}
// ------------------------------------------------------------------------


/**
 * get_record_list_advanced_search
 *
 * Returns the record list advanced search where clause
 *
 * 
 * @param  array    advanced search fields  
 * @param  array   custom search strings for dates etc  
 * @return  string
 */
if ( ! function_exists('get_record_list_advanced_search'))
{
    function get_record_list_advanced_search($advanced_search_fields, $custom_advanced_search = array())
    { 
        $CI =& get_instance();
        $search_handler = array('search_clause' => array(), 'data' => array(), 'search_query_string' => '', 'search_clause_string' => '');
        $search_handler['search_query_string'] = "frm_advanced_search=1";
        $i = 0;
        $flag = FALSE;
        foreach($advanced_search_fields as $key => $search_field) 
        {
            $search_value = $CI->input->get($search_field['input_field_name']); 
            // check if search_value is array
            if(is_array($search_value))
            { 
                foreach ($search_value as $key => $value) {
                    $search_handler['search_query_string'] .= "&" . $search_field['input_field_name'] . "[]=$value";
                    $search_handler['data'][$search_field['input_field_name']][] = $value;
                }
            }
            else
            { 
                $search_handler['search_query_string'] .= "&" . $search_field['input_field_name'] . "=$search_value"; 
                $search_handler['data'][$search_field['input_field_name']] = $search_value;
            }
            
            if( ! empty($search_value)) 
            {
                if($flag === FALSE)
                {
                    $search_handler['search_clause_string'] = "("; 
                    $flag = TRUE;
                }
                // create array of fields and values for passing array of search params in functions like or_like $this->db->or_like($where_clause);
                $search_handler['search_clause'][$search_field['table_attribute']] = $search_value;

                // create where clause string for $this->db->where($where_string, NULL, FALSE);
                if($i !== 0)
                {
                    if(isset($search_field['where_clause_operator']) && ! empty($search_field['where_clause_operator']))
                    {
                        $search_handler['search_clause_string'] .=  " " . $search_field['where_clause_operator'] . " ";
                    }
                    else
                        $search_handler['search_clause_string'] .= " AND ";
                }

                if($search_field['operator'] == 'like')
                {
                    $search_handler['search_clause_string'] .= $search_field['table_attribute'] . " LIKE '%" . $search_value . "%'";
                }
                elseif($search_field['operator'] == 'in')
                {
                    $search_handler['search_clause_string'] .= $search_field['table_attribute'] . " IN (" . $search_value . ")";
                }
                else
                {
                    $search_handler['search_clause_string'] .= $search_field['table_attribute'] . $search_field['operator'] . "'" . $search_value . "'";
                } 
                $i++;
            } 
        }
        // Custom Advanced Search
        if(sizeof($custom_advanced_search))
        {    
            if($flag === FALSE)
            {
                $search_handler['search_clause_string'] = "("; 
                $flag = TRUE;
            }
            $x = 0;
            foreach ($custom_advanced_search as $key => $advanced_search) 
            {
                if($i > 0 || $x > 0)
                {
                    if(isset($advanced_search['where_clause_operator']) && ! empty($advanced_search['where_clause_operator']))
                    {
                        $search_handler['search_clause_string'] .=  " " . $advanced_search['where_clause_operator'] . " ";
                    }
                    else
                    {
                        $search_handler['search_clause_string'] .= " AND ";
                    }
                }
                $search_handler['search_clause_string'] .= $advanced_search['search_string'];
                $x++; 
            } 
            
        }
        // End: Custom Advanced Search
        if($flag === TRUE)
        {
            $search_handler['search_clause_string'] .= ")";
        }

        #echo $search_handler['search_clause_string']; die;
        return $search_handler;
    }
}

/* End of file pagination_helper.php */
/* Location: ./application/helpers/pagination_helper.php */