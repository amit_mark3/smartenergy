<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * themes_url
 *
 * Returns the current theme url
 *
 * @param	
 * @return	string
 */
if ( ! function_exists('themes_url'))
{
	function themes_url()
	{
        $CI =& get_instance();
        $active_theme = $CI->config->item('admin_theme'); 
		return base_url().'themes/'.$active_theme;
	}
}
// ------------------------------------------------------------------------

/**
 * get_front_theme_url
 *
 * Returns the current theme url for front end
 *
 * @param   
 * @return  string
 */
if ( ! function_exists('get_front_theme_url'))
{
    function get_front_theme_url()
    {
        $CI =& get_instance();
        $active_theme = $CI->config->item('front_theme'); 
        return base_url().'themes/'.$active_theme;
    }
}

// ------------------------------------------------------------------------

/**
 * get_vendors_url
 *
 * Returns the current theme url for front end
 *
 * @param   
 * @return  string
 */
if ( ! function_exists('get_vendors_url'))
{
    function get_vendors_url()
    {
        return base_url().'vendors';
    }
}

// ------------------------------------------------------------------------

/**
 * get_front_js_url
 *
 * Returns the current theme js url for front end
 *
 * @param   
 * @return  string
 */
if ( ! function_exists('get_front_js_url'))
{
    function get_front_js_url()
    {
        $CI =& get_instance();
        $active_theme = $CI->config->item('front_theme'); 
        return base_url().'assets/'.$active_theme.'/js/';
    }
}
// ------------------------------------------------------------------------

/**
 * assets_url
 *
 * Returns the assets url
 *
 * @param   
 * @return  string
 */
if ( ! function_exists('assets_url'))
{
    function assets_url()
    {
        $CI =& get_instance();
        return base_url().'assets';
    }
}
// ------------------------------------------------------------------------


/**
 * generate_breadcrumb
 *
 * takes an array of links and return the generated breadcrumb
 *
 * @param   array
 * @return  string
 */
if ( ! function_exists('generate_breadcrumb'))
{
    function generate_breadcrumb($breadcrumb_elements, $main_title = '')
    {
        $CI =& get_instance();
        //point to end of the array
        end($breadcrumb_elements);
        //fetch key of the last element of the array.
        $last_element_key = key($breadcrumb_elements);
        $breadcrumb_html = '
            <h1>'.$main_title.'</h1>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="'. site_url() .'">Site Home</a>
            <i class="fa fa-angle-right"></i>'; 
        foreach ($breadcrumb_elements as $key => $element) 
        {
            if($key == $last_element_key) 
            {
                //during array iteration this condition states the last element.
                $breadcrumb_html .= '<span class="active">'. $element['link_text'] .'</span>';
            }
            else
            {
                $breadcrumb_html .= '
                <span>
                    <a href="'. $element['link'] .'">'. $element['link_text'] .'</a>
                    <i class="fa fa-angle-right"></i>
                </span>';
            }
        } 
        
        return $breadcrumb_html;
    }
} 

// ------------------------------------------------------------------------

/**
 * get_current_module_name
 *
 * Returns the current module name
 *
 * @param 
 * @return  string
 */
if ( ! function_exists('get_current_module_name'))
{
    function get_current_module_name()
    {
        $CI =& get_instance();
        // Modular Separation / Modular Extensions has been detected
        if (method_exists($CI->router, 'fetch_module')) {
            $module = $CI->router->fetch_module();
            return (!empty($module)) ? $module : '';
        }
        return '';
    }
}
// ------------------------------------------------------------------------


/**
 * get_success_message 
 *
 * Returns the success message
 *
 * @param string
 * @return  string
 */
if ( ! function_exists('get_success_message'))
{
    function get_success_message($message = NULL)
    {
        if($message === NULL) return '';
        return '
        <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            '. $message .'            
        </div>';
    }
}
// ------------------------------------------------------------------------



/**
 * get_error_message 
 *
 * Returns the error message
 *
 * @param string
 * @return  string
 */
if ( ! function_exists('get_error_message'))
{
    function get_error_message($message = NULL)
    {
        if($message === NULL) return '';
        return '
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            '. $message .'            
        </div>';
    }
}
// ------------------------------------------------------------------------

/**
 * get_validation_error_message 
 *
 * Returns the validation error message
 *
 * @param string
 * @return  string
 */
if ( ! function_exists('get_validation_error_message'))
{
    function get_validation_error_message($message = FALSE)
    {
        if($message === FALSE) return '';
        return '
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <ul>
                '. $message .' 
            </ul>           
        </div>';
    }
}
// ------------------------------------------------------------------------

/**
 * get_dropdown_html
 *
 * Returns the dropdown html
 *
 * 
 * @param   string      (name of the dropdown) 
 * @param   array       (options of the drop down)
 * @param   string/int  (default selected option of dropdown)
 * @param   string      (other attibutes of drop down like id, class, onchange)
 * @param   sting/int   (first option key of dropdown)
 * @param   sting       (first option value of dropdown)
 * @param   stirng/int  (data array element to be as option key if the data array passed is multidimentional)
 * @param   stirng      (data array element to be as option value if the data array passed is multidimentional)
 * @param   string      (any char, alpahent or number to append in after option value like #, -)
 * @param   string      (any array element to append in after option value char like #49)
 * @return  string      (any element in data array {if multidimentional} to be appended in after option value)
 */
if ( ! function_exists('get_dropdown_html'))
{
    function get_dropdown_html($name, $data, $pre_selected = '', $params = '', $first_option_key = '', $first_option_value = '', $data_key = '', $data_value = '', $add_value_char = '', $add_value_element = '')
    {
        
        $CI =& get_instance();
        $CI->load->helper('form');
        $options = array();
        if( !empty($first_option_value))
        {
            $options[$first_option_key] = $first_option_value;
        }
        if( !empty($data_key) && !empty($data_value))
        {
           foreach ($data as $key => $value) {
                $option_value = $value[$data_value]; 
                if( !empty($add_value_char))
                    $option_value .= $add_value_char;
                if( !empty($add_value_element))
                    $option_value .= $value[$add_value_element]; 
                $options[$value[$data_key]] = $option_value; 
            } 
        }
        else
        {
            //$options = array_merge($options , $data); // array merge changes the key for $data
            $options = $options + $data; 
        } 
        $selected = ($pre_selected) ? $pre_selected : '';
        $changed = FALSE;
        foreach ($options as $key => $value) {
            $changed = set_select($name, $key);
            if( !empty($changed))
            {
                $selected = $key;
                break;
            } 
        }   
        return form_dropdown($name, $options, $selected, $params);  
    }
} 

// ------------------------------------------------------------------------

/**
 * get_status_dropdown_html
 *
 * Returns the status dropdown html
 *
 * 
 * @param   string      (name of the dropdown) 
 * @param   array       (options of the drop down)
 * @param   string/int  (default selected option of dropdown)
 * @param   string      (other attibutes of drop down like id, class, onchange)
 * @param   sting/int   (first option key of dropdown)
 * @param   sting       (first option value of dropdown)
 * @param   stirng/int  (data array element to be as option key if the data array passed is multidimentional)
 * @param   stirng      (data array element to be as option value if the data array passed is multidimentional)
 * @param   string      (any char, alpahent or number to append in after option value like #, -)
 * @param   string      (any array element to append in after option value char like #49)
 * @return  string      (any element in data array {if multidimentional} to be appended in after option value)
 */
if ( ! function_exists('get_status_dropdown_html'))
{
    function get_status_dropdown_html($params = '', $selected = '')
    {
        $CI =& get_instance();
        $CI->load->helper('form');
        $options = $CI->config->item('status_options'); 
        return form_dropdown('status', $options, $selected, $params);  
    }
}
// ------------------------------------------------------------------------
/* End of file utility_helper.php */
/* Location: ./application/helpers/utility_helper.php */