<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * get_countries_dropdown_html
 *
 * Returns the countries dropdown
 *
 * @param   string // name of drop down
 * @param   string // pre selected option of drop down
 * @param   string // attributes of drop down
 * @param   string // first option key
 * @param   string // first option value
 * @param   string // add value character in dropdown option value
 * @param   string // add value element for dropdown option value
 * @return  string 
 */
if ( ! function_exists('get_countries_dropdown_html'))
{
    function get_countries_dropdown_html($name, $pre_selected = '', $params = '', $first_option_key = '', $first_option_value = '',$add_value_char = '', $add_value_element = '')
    {
        $CI =& get_instance();
        $CI->load->model('common_model'); 
        
        $countries = $CI->common_model->get_countries(0, 0, '', '', FALSE);
        return get_dropdown_html($name, $countries, $pre_selected, $params, $first_option_key, $first_option_value, 'id', 'name', $add_value_char, $add_value_element);
    }
}

// ------------------------------------------------------------------------


/**
 * get_states_dropdown_html
 *
 * Returns the states dropdown
 *
 * @param   string // name of drop down
 * @param   string // pre selected option of drop down
 * @param   string // attributes of drop down
 * @param   string // first option key
 * @param   string // first option value
 * @param   string // add value character in dropdown option value
 * @param   string // add value element for dropdown option value
 * @return  string 
 */
if ( ! function_exists('get_states_dropdown_html'))
{
    function get_states_dropdown_html($name, $pre_selected = '', $params = '', $first_option_key = '', $first_option_value = '',$add_value_char = '', $add_value_element = '', $where_clause = array(), $where_string = '')
    {
        $CI =& get_instance();
        $CI->load->model('common_model'); 
        
        $states = $CI->common_model->get_states(0, 0, '', '', FALSE, $where_clause, $where_string);
        return get_dropdown_html($name, $states, $pre_selected, $params, $first_option_key, $first_option_value, 'id', 'name', $add_value_char, $add_value_element);
    }
}


// ------------------------------------------------------------------------


/**
 * get_districts_dropdown_html
 *
 * Returns the states dropdown
 *
 * @param   string // name of drop down
 * @param   string // pre selected option of drop down
 * @param   string // attributes of drop down
 * @param   string // first option key
 * @param   string // first option value
 * @param   string // add value character in dropdown option value
 * @param   string // add value element for dropdown option value
 * @return  string 
 */
if ( ! function_exists('get_districts_dropdown_html'))
{
    function get_districts_dropdown_html($name, $pre_selected = '', $params = '', $first_option_key = '', $first_option_value = '',$add_value_char = '', $add_value_element = '', $where_clause = array(), $where_string = '')
    {
        $CI =& get_instance();
        $CI->load->model('common_model'); 
        
        $states = $CI->common_model->get_districts(0, 0, '', '', FALSE, $where_clause, $where_string);
        return get_dropdown_html($name, $states, $pre_selected, $params, $first_option_key, $first_option_value, 'id', 'name', $add_value_char, $add_value_element);
    }
}

// ------------------------------------------------------------------------

/* End of file countries_helper.php */
/* Location: ./application/helpers/countries_helper.php */