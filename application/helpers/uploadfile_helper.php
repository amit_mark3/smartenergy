<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @param   string // name of drop down
 * @param   string // pre selected option of drop down
 * @param   string // attributes of drop down
 * @param   string // first option key
 * @param   string // first option value
 * @param   string // add value character in dropdown option value
 * @param   string // add value element for dropdown option value
 * @return  string 
 */
if ( ! function_exists('uploadfile_image'))
{
    function uploadfile_image($field,$cur_time = '', $directory = '', $file_type = '')
    {
		$CI =& get_instance();
		$time = $cur_time == ''?time():$cur_time;
		$file_name = trim($_FILES[$field]['name']);
		$file_name = preg_replace('/[^a-zA-Z0-9\-\._]/','', $file_name);
		$fname = str_replace(" ","",$file_name);
		$new_name = $time.'_'.$fname;
		$allowed_types = $file_type==''?'gif|jpg|jpeg|png':$file_type;
	
	$dir = '/cms/uploads/images/';
	$uploaddir = $directory == ''?$dir:$directory;
	//$config['sess_save_path'] = './temp_sess/';
	//$uploaddir = '/uploads/images/';
	//$file = "." . $uploaddir . basename($file_name);    
	if(!is_dir("." . $uploaddir . date('Y')))
	mkdir("." . $uploaddir . date('Y'), 0777, true);	
	if(!is_dir("." . $uploaddir . date('Y') . "/" . date('m')))
	mkdir("." . $uploaddir . date('Y') . "/" . date('m'), 0777, true);
	if(!is_dir("." . $uploaddir . date('Y') . "/" . date('m') . "/" . date('d')))
	mkdir("." . $uploaddir . date('Y') . "/" . date('m') . "/" . date('d'), 0777, true);
	//echo $uploaddir; 
	 $path =  $uploaddir.date('Y') . "/" . date('m') . "/" . date('d').'/';
//'max_width' => '1024',
//'max_height' => '1024',
			
		$config = array (
			'upload_path' => './'.$path,
			'allowed_types' => $allowed_types,
			'max_size' => '3024',
			'file_name' => $new_name
			
		);
		//'encrypt_name' => TRUE,
		
		$CI->load->library ( 'upload' ); //load codeigniter libraries
		$CI->upload->initialize($config);
		$name_array = array ();
		$count = count ( $_FILES[ $field ][ 'size' ] );
		if ( count ( $_FILES[ $field ] ) == count ( $_FILES[ $field ], COUNT_RECURSIVE ) ) //if only one image is present
		{
			//echo $field; 
			if ( !$CI->upload->do_upload ( $field ) )
			{
				$error = array('error' => $CI->upload->display_errors());
				echo 'Invalid file type.';
				//print_r($error);die;
				return FALSE;
			}else{
				$data = $CI->upload->data ();
				$data['save_path'] = $path.$new_name;
				$data['file_name'] = $new_name;
				//print_r($data);die;
				return $data; //return the uploaded file name
			}
		}
		else //if multiple images selected
		{
			foreach ( $_FILES as $key => $value )
			{
				for ( $s = 0; $s < $count; $s++ )
				{
					$_FILES[ $field ][ 'name' ] = $value[ 'name' ][ $s ];
					$_FILES[ $field ][ 'type' ] = $value[ 'type' ][ $s ];
					$_FILES[ $field ][ 'tmp_name' ] = $value[ 'tmp_name' ][ $s ];
					$_FILES[ $field ][ 'error' ] = $value[ 'error' ][ $s ];
					$_FILES[ $field ][ 'size' ] = $value[ 'size' ][ $s ];
					if ( $CI->upload->do_upload () )
					{
						$data = $CI->upload->data ();
						$data['save_path'] = $path.$new_name;
						$data['file_name'] = $new_name;
						$name_array[ ] = $data;
					}
					else
					{
						$error = array('error' => $CI->upload->display_errors());
						echo 'Invalid file type.';
						//print_r($error);die;
						return FALSE;
					}
				}
				return $name_array;//return the names of images uploaded
			}
		}
        
    }
}
// Check File Type
if ( ! function_exists('uploadfile_type'))
{
    function uploadfile_type($field, $allowed_file_type = '',$allowed_file_size = ''){
		///$CI =& get_instance();
		//$CI->load->library ( 'upload' ); //load codeigniter libraries
		//$upload=$CI->upload;
		$file_path = $_FILES[$field]['tmp_name'];
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime_type = finfo_file($finfo, $file_path);
		finfo_close($finfo);
		$file_size = $_FILES[ $field ][ 'size' ];
		//print_r($mime_type);die;
		//$allowed = $allowed_file_type==''?array("image/jpeg", "image/gif", "image/png", "image/jpg"):$allowed_file_type;
		//For Video
		$allowed_video_file_type = array('video/x-ms-asf','video/x-ms-asf-plugin', 'application/x-troff-msvideo', 'video/avi','video/msvideo','video/x-msvideo','video/avs-video','video/x-dv', 'video/fli','video/x-fli','video/x-atomic3d-feature','video/gl','video/x-gl','video/mpeg','video/x-mpeg','video/x-mpeq2a','video/x-sgi-movie','video/x-qtc','video/quicktime','video/vnd.rn-realvideo', 'video/x-scm','video/vdo','video/vivo','video/vnd.vivo','video/x-amt-demorun','video/x-flv','video/flv', 'video/webm', 'video/mp4', 'video/3gp', 'video/ogv','video/mpg','video/mpe','video/qt','video/mov');
		// For Image
		$allowed_image_file_type = array("image/jpeg", "image/gif", "image/png", "image/jpg");
		
		$allowed = $allowed_file_type=='1'?array("image/jpeg", "image/gif", "image/png", "image/jpg"):$allowed_file_type;
		
		$allowed_file_size = $allowed_file_size==''?'3':$allowed_file_size;
		$data = array();
		$allowed  = '';
		if(in_array($mime_type, $allowed_video_file_type)) {
			$allowed = $allowed_video_file_type;
			$media_type = '1';//1 = For Video ,0 = Photo
			$allowed_file_size = '51200'; //3MB
		}	
		if(in_array($mime_type, $allowed_image_file_type)) {
			$allowed = $allowed_image_file_type;
			$media_type = '0';//1 = For Video ,0 = Photo
			$allowed_file_size = '3000'; //3MB
		}
		
		if(!in_array($mime_type, $allowed)) {
			//echo 'asdf';die;
			$data['error'] = 'Invalid file type.';
		}elseif($allowed_file_size>$file_size){
			$data['error'] = 'Invalid file size.';
		}else{
			$data['file_type'] = $mime_type;
			$data['media_type'] = $media_type;
			$data['file_size'] = $_FILES[ $field ][ 'size' ];
			$data['error'] = '';			
		}
		//print_r($data);die;
		return $data; //return the uploaded file name
	}
}
//Default For US East (N. Virginia)	us-east-1	appstream2.us-east-1.amazonaws.com
// load upload file in s3
if ( ! function_exists('uploadfile_s3'))
{
    function uploadfile_s3($field,$cur_time = '', $directory = '', $file_type = '')
    {
		$CI =& get_instance();
		$CI->load->library('S3'); //load codeigniter libraries
		$s3 = $CI->s3;
		// AWS access info
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAILQ7L67BQE7TY3SQ');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3->setAuth(awsAccessKey, awsSecretKey);
		//$bucket = 'Iffco-input-videos';
		$bucket = 'iffco-input-videos-sgpr';
		// List your buckets:
		//echo "S3::listBuckets(): ".print_r($s3->listBuckets(), 1)."\n";
		
		$time = $cur_time == ''?time():$cur_time;
		$file_name = trim($_FILES[$field]['name']);
		$file_name = preg_replace('/[^a-zA-Z0-9\-\._]/','', $file_name);
		$fname = str_replace(" ","",$file_name);
		$filename = $time.'_'.$fname;
		$video_file_name = $filename;//md5($filename).'.m3u8';
		//$video_file_name = md5($filename).'.m3u8';
		$new_name = $file_type=='1'?$video_file_name:$filename; //$file_type=='1' only for video
		
		//$allowed_types = $file_type==''?'gif|jpg|jpeg|png':$file_type;
		//$file_path_name = "uploads/images/" . date('Y') . "/" . date('m') . "/" . date('d') . "/" . preg_replace("/[^a-z0-9\.]/", "-", strtolower($new_name));
		$file_path_name = "input/" . strtolower($new_name);
		//echo baseName($file_path_name); die;
		//$s3->setEndpoint('s3-ap-south-1.amazonaws.com');
		// Set for Singapur Region
		$elocation = 'ap-southeast-1';
		$s3->setEndpoint('s3-ap-southeast-1.amazonaws.com');
		//if ($s3->putBucket($bucket, S3::ACL_PUBLIC_READ, $elocation)) {
			//echo "Created bucket {$bucket}".PHP_EOL;
			if ($s3->putObjectFile($_FILES[$field]['tmp_name'], $bucket, $file_path_name, $s3->ACL_PUBLIC_READ)) {
				/*echo "S3::putObjectFile(): File copied to {$bucket}/".baseName($file_path_name).PHP_EOL;			
				// Get the contents of our bucket
				$contents = $s3->getBucket($bucket);
				echo "S3::getBucket(): Files in bucket {$bucket}: ".print_r($contents, 1);
				// Get object info
				$info = $s3->getObjectInfo($bucket, baseName($file_path_name));
				echo "S3::getObjectInfo(): Info for {$bucket}/".baseName($file_path_name).': '.print_r($info, 1);
				*/
				$data['save_path'] = $file_path_name;
				$data['file_name'] = $new_name;
				//print_r($data);die;
				return $data; //return the uploaded file name
			
			}else{
				echo "S3::putObjectFile(): Failed to copy file\n";
			}
		/*}else{
			echo "S3::putBucket(): Unable to create bucket (it may already exist and/or be owned by someone else)\n";
			//print_r($error);die;
			//return FALSE;
		}*/
	}	
	
}

if ( ! function_exists('uploadfile_s3mumbai')){
    function uploadfile_s3mumbai($field,$cur_time = '', $directory = '', $file_type = '')
    {
		$CI =& get_instance();
		//$CI->load->library('Amazons3'); //load codeigniter libraries
		$CI->load->library('amazons3');
		$CI->amazons3->up(); 
		//print_R($t);
		//$s3 = $CI->Amazons3;
		//$s3->updddd();
		//echo 'sdafsdf';die;
		
	}	
	
}
// ------------------------------------------------------------------------


/* End of file uploadfile_helper.php */
/* Location: ./application/helpers/uploadfile_helper.php */